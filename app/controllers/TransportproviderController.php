<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;


class TransportproviderController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for transportprovider
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, 'Transportprovider', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $transportprovider = Transportprovider::find($parameters);
        if (count($transportprovider) == 0) {
            $this->flash->notice("The search did not find any transportprovider");

            $this->dispatcher->forward([
                "controller" => "transportprovider",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $transportprovider,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a transportprovider
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $transportprovider = Transportprovider::findFirstByid($id);
            if (!$transportprovider) {
                $this->flash->error("transportprovider was not found");

                $this->dispatcher->forward([
                    'controller' => "transportprovider",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $transportprovider->id;

            $this->tag->setDefault("id", $transportprovider->id);
            $this->tag->setDefault("transportproviderid", $transportprovider->transportproviderid);
            $this->tag->setDefault("companyname", $transportprovider->companyname);
            $this->tag->setDefault("phoneno", $transportprovider->phoneno);
            $this->tag->setDefault("addressno", $transportprovider->addressno);
            $this->tag->setDefault("streetname", $transportprovider->streetname);
            $this->tag->setDefault("eircode", $transportprovider->eircode);
            $this->tag->setDefault("city", $transportprovider->city);
            $this->tag->setDefault("county", $transportprovider->county);
            $this->tag->setDefault("email", $transportprovider->email);
            $this->tag->setDefault("services", $transportprovider->services);
            $this->tag->setDefault("vehicletypes", $transportprovider->vehicletypes);
            
        }
    }

    /**
     * Creates a new transportprovider
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "transportprovider",
                'action' => 'index'
            ]);

            return;
        }

        $transportprovider = new Transportprovider();
        $transportprovider->transportproviderid = $this->request->getPost("transportproviderid");
        $transportprovider->companyname = $this->request->getPost("companyname");
        $transportprovider->phoneno = $this->request->getPost("phoneno");
        $transportprovider->addressno = $this->request->getPost("addressno");
        $transportprovider->streetname = $this->request->getPost("streetname");
        $transportprovider->eircode = $this->request->getPost("eircode");
        $transportprovider->city = $this->request->getPost("city");
        $transportprovider->county = $this->request->getPost("county");
        $transportprovider->email = $this->request->getPost("email", "email");
        $transportprovider->services = $this->request->getPost("services");
        $transportprovider->vehicletypes = $this->request->getPost("vehicletypes");
        

        if (!$transportprovider->save()) {
            foreach ($transportprovider->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "transportprovider",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("transportprovider was created successfully");

        $this->dispatcher->forward([
            'controller' => "transportprovider",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a transportprovider edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "transportprovider",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $transportprovider = Transportprovider::findFirstByid($id);

        if (!$transportprovider) {
            $this->flash->error("transportprovider does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "transportprovider",
                'action' => 'index'
            ]);

            return;
        }

        $transportprovider->transportproviderid = $this->request->getPost("transportproviderid");
        $transportprovider->companyname = $this->request->getPost("companyname");
        $transportprovider->phoneno = $this->request->getPost("phoneno");
        $transportprovider->addressno = $this->request->getPost("addressno");
        $transportprovider->streetname = $this->request->getPost("streetname");
        $transportprovider->eircode = $this->request->getPost("eircode");
        $transportprovider->city = $this->request->getPost("city");
        $transportprovider->county = $this->request->getPost("county");
        $transportprovider->email = $this->request->getPost("email", "email");
        $transportprovider->services = $this->request->getPost("services");
        $transportprovider->vehicletypes = $this->request->getPost("vehicletypes");
        

        if (!$transportprovider->save()) {

            foreach ($transportprovider->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "transportprovider",
                'action' => 'edit',
                'params' => [$transportprovider->id]
            ]);

            return;
        }

        $this->flash->success("transportprovider was updated successfully");

        $this->dispatcher->forward([
            'controller' => "transportprovider",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a transportprovider
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $transportprovider = Transportprovider::findFirstByid($id);
        if (!$transportprovider) {
            $this->flash->error("transportprovider was not found");

            $this->dispatcher->forward([
                'controller' => "transportprovider",
                'action' => 'index'
            ]);

            return;
        }

        if (!$transportprovider->delete()) {

            foreach ($transportprovider->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "transportprovider",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("transportprovider was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "transportprovider",
            'action' => "index"
        ]);
    }

}

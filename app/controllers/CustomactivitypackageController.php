<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Customactivitypackage;

class CustomactivitypackageController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }
	
	/**
     * display cart action
     */
	
	
	public function displayCartAction()
	
	{
	

            $user = $this->session->get('user');
            if (!$user) {
                echo ("Please login to Proceed with this page!");

                $this->dispatcher->forward([
                    'controller' => "user",
                    'action' => 'index'
                ]);

                return;
            }
		$cust = $this->session->get('user');
		$custID = $cust->getID();
		$allBookings = Customactivitypackage::findByUserid($custID);
		$totalNoOfActivities = $allBookings->count();
		$this->view->myBookings=$allBookings;
		
	}


    /**
     * Searches for customactivitypackage
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Customactivitypackage', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $customactivitypackage = Customactivitypackage::find($parameters);
        if (count($customactivitypackage) == 0) {
            $this->flash->notice("The search did not find any customactivitypackage");

            $this->dispatcher->forward([
                "controller" => "customactivitypackage",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $customactivitypackage,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }
	
	 /**
     * Displays the success page
     */
	public function successcreateAction()
    {

    }

    /**
     * Edits a customactivitypackage
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $customactivitypackage = Customactivitypackage::findFirstByid($id);
            if (!$customactivitypackage) {
                $this->flash->error("customactivitypackage was not found");

                $this->dispatcher->forward([
                    'controller' => "customactivitypackage",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $customactivitypackage->getId();

            $this->tag->setDefault("id", $customactivitypackage->getId());
            $this->tag->setDefault("scheduledactivityid", $customactivitypackage->getScheduledactivityid());
            $this->tag->setDefault("userid", $customactivitypackage->getUserid());
            
        }
    }

    /**
     * Creates a new customactivitypackage
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'index'
            ]);

            return;
        }

            $user = $this->session->get('user');
            if (!$user) {
                echo ("Please login to Proceed with this page!");

                $this->dispatcher->forward([
                    'controller' => "user",
                    'action' => 'index'
                ]);

                return;
            }
			
        $customactivitypackage = new Customactivitypackage();
        $customactivitypackage->setscheduledactivityid($this->request->getPost("scheduledactivityid"));
        $customactivitypackage->setuserid($this->session->get('user')->getID());
        

        if (!$customactivitypackage->save()) {
            foreach ($customactivitypackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("Total cost of your activity is : €" .$customactivitypackage->getScheduledactivity()->getActivitytype()->getCost());
		return $this->dispatcher->forward(["controller" => "customactivitypackage","action" => "successcreate"]);
		
		
		
    }

    /**
     * Saves a customactivitypackage edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $customactivitypackage = Customactivitypackage::findFirstByid($id);

        if (!$customactivitypackage) {
            $this->flash->error("customactivitypackage does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'index'
            ]);

            return;
        }

        $customactivitypackage->setscheduledactivityid($this->request->getPost("scheduledactivityid"));
        $customactivitypackage->setuserid($this->request->getPost("userid"));
        

        if (!$customactivitypackage->save()) {

            foreach ($customactivitypackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'edit',
                'params' => [$customactivitypackage->getId()]
            ]);

            return;
        }

        $this->flash->success("customactivitypackage was updated successfully");

        $this->dispatcher->forward([
            'controller' => "customactivitypackage",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a customactivitypackage
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $customactivitypackage = Customactivitypackage::findFirstByid($id);
        if (!$customactivitypackage) {
            $this->flash->error("customactivitypackage was not found");

            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'index'
            ]);

            return;
        }

        if (!$customactivitypackage->delete()) {

            foreach ($customactivitypackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customactivitypackage",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("customactivitypackage was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "customactivitypackage",
            'action' => "index"
        ]);
    }

}

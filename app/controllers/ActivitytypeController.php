<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Activitytype;

class ActivitytypeController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

	public function displayGridAction()
    {
        $this->view->activitytypes = activitytype::find();


    }
    /**
     * Searches for activitytype
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Activitytype', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $activitytype = Activitytype::find($parameters);
        if (count($activitytype) == 0) {
            $this->flash->notice("The search did not find any activitytype");

            $this->dispatcher->forward([
                "controller" => "activitytype",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $activitytype,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a activitytype
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $activitytype = Activitytype::findFirstByid($id);
            if (!$activitytype) {
                $this->flash->error("activitytype was not found");

                $this->dispatcher->forward([
                    'controller' => "activitytype",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $activitytype->getId();

            $this->tag->setDefault("id", $activitytype->getId());
            $this->tag->setDefault("product", $activitytype->getProduct());
            $this->tag->setDefault("activitylevel", $activitytype->getActivitylevel());
            $this->tag->setDefault("activitybase", $activitytype->getActivitybase());
            $this->tag->setDefault("cost", $activitytype->getCost());
            $this->tag->setDefault("insurance", $activitytype->getInsurance());
            $this->tag->setDefault("description", $activitytype->getDescription());
            $this->tag->setDefault("activitypic", $activitytype->getActivitypic());
            
        }
    }

    /**
     * Creates a new activitytype
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'index'
            ]);

            return;
        }

        $activitytype = new Activitytype();
        $activitytype->setproduct($this->request->getPost("product"));
        $activitytype->setactivitylevel($this->request->getPost("activitylevel"));
        $activitytype->setactivitybase($this->request->getPost("activitybase"));
        $activitytype->setcost($this->request->getPost("cost"));
        $activitytype->setinsurance($this->request->getPost("insurance"));
        $activitytype->setdescription($this->request->getPost("description"));
        $activitytype->setactivitypic($this->request->getPost("activitypic"));
        

        if (!$activitytype->save()) {
            foreach ($activitytype->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("activitytype was created successfully");

        $this->dispatcher->forward([
            'controller' => "activitytype",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a activitytype edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $activitytype = Activitytype::findFirstByid($id);

        if (!$activitytype) {
            $this->flash->error("activitytype does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'index'
            ]);

            return;
        }

        $activitytype->setproduct($this->request->getPost("product"));
        $activitytype->setactivitylevel($this->request->getPost("activitylevel"));
        $activitytype->setactivitybase($this->request->getPost("activitybase"));
        $activitytype->setcost($this->request->getPost("cost"));
        $activitytype->setinsurance($this->request->getPost("insurance"));
        $activitytype->setdescription($this->request->getPost("description"));
        $activitytype->setactivitypic($this->request->getPost("activitypic"));
        

        if (!$activitytype->save()) {

            foreach ($activitytype->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'edit',
                'params' => [$activitytype->getId()]
            ]);

            return;
        }

        $this->flash->success("activitytype was updated successfully");

        $this->dispatcher->forward([
            'controller' => "activitytype",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a activitytype
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $activitytype = Activitytype::findFirstByid($id);
        if (!$activitytype) {
            $this->flash->error("activitytype was not found");

            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'index'
            ]);

            return;
        }

        if (!$activitytype->delete()) {

            foreach ($activitytype->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "activitytype",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("activitytype was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "activitytype",
            'action' => "index"
        ]);
    }

}

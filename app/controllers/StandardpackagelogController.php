<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Standardpackagelog;

class StandardpackagelogController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }
	
	public function displayCalendarAction($standardpackageid, $searchdate)
	{
		$fcCollection = $this->assets->collection("fullCalendar");
		$fcCollection->addJs('js/moment.min.js');
		$fcCollection->addJs('js/fullcalendar.min.js');
		$fcCollection->addCss('css/fullcalendar.min.css');
		$this->view->standardpackageid=$standardpackageid; 
		$this->view->searchdate=$searchdate;
	}
	
	public function jsonAction($standardpackageid)
	{
		//$this->view->disable();

		$packevents = Packevent::findBystandardpackageid($standardpackageid);
		$this->response->resetHeaders();
		$this->response->setContentType('application/json', 'UTF-8');
		$this->response->setContent(json_encode($packevents));
		return $this->response->send();
	}
    /**
     * Searches for standardpackagelog
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Standardpackagelog', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $standardpackagelog = Standardpackagelog::find($parameters);
        if (count($standardpackagelog) == 0) {
            $this->flash->notice("The search did not find any standardpackagelog");

            $this->dispatcher->forward([
                "controller" => "standardpackagelog",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $standardpackagelog,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a standardpackagelog
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $standardpackagelog = Standardpackagelog::findFirstByid($id);
            if (!$standardpackagelog) {
                $this->flash->error("standardpackagelog was not found");

                $this->dispatcher->forward([
                    'controller' => "standardpackagelog",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $standardpackagelog->getId();

            $this->tag->setDefault("id", $standardpackagelog->getId());
            $this->tag->setDefault("scheduleactivitylogid", $standardpackagelog->getScheduleactivitylogid());
            $this->tag->setDefault("standardpackageid", $standardpackagelog->getStandardpackageid());
            
        }
    }

    /**
     * Creates a new standardpackagelog
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "standardpackagelog",
                'action' => 'index'
            ]);

            return;
        }

        $standardpackagelog = new Standardpackagelog();
        $standardpackagelog->setscheduleactivitylogid($this->request->getPost("scheduleactivitylogid"));
        $standardpackagelog->setstandardpackageid($this->request->getPost("standardpackageid"));
        

        if (!$standardpackagelog->save()) {
            foreach ($standardpackagelog->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardpackagelog",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("standardpackagelog was created successfully");

        $this->dispatcher->forward([
            'controller' => "standardpackagelog",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a standardpackagelog edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "standardpackagelog",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $standardpackagelog = Standardpackagelog::findFirstByid($id);

        if (!$standardpackagelog) {
            $this->flash->error("standardpackagelog does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "standardpackagelog",
                'action' => 'index'
            ]);

            return;
        }

        $standardpackagelog->setscheduleactivitylogid($this->request->getPost("scheduleactivitylogid"));
        $standardpackagelog->setstandardpackageid($this->request->getPost("standardpackageid"));
        

        if (!$standardpackagelog->save()) {

            foreach ($standardpackagelog->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardpackagelog",
                'action' => 'edit',
                'params' => [$standardpackagelog->getId()]
            ]);

            return;
        }

        $this->flash->success("standardpackagelog was updated successfully");

        $this->dispatcher->forward([
            'controller' => "standardpackagelog",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a standardpackagelog
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $standardpackagelog = Standardpackagelog::findFirstByid($id);
        if (!$standardpackagelog) {
            $this->flash->error("standardpackagelog was not found");

            $this->dispatcher->forward([
                'controller' => "standardpackagelog",
                'action' => 'index'
            ]);

            return;
        }

        if (!$standardpackagelog->delete()) {

            foreach ($standardpackagelog->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardpackagelog",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("standardpackagelog was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "standardpackagelog",
            'action' => "index"
        ]);
    }

}

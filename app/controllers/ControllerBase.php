<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
	public function initialize()
		{
		$this->assets->addCss('css/font-awesome.min.css');
		$this->assets->addCss('css/animate.css');
		$this->assets->addCss('css/hover-min.css');
		$this->assets->addCss('css/datepicker.css');
		$this->assets->addCss('css/owl.carousel.min.css');
		$this->assets->addCss('css/owl.theme.default.min.css');
		$this->assets->addCss('css/jquery-ui.min.css');
		$this->assets->addCss('css/bootstrap.min.css');
		$this->assets->addCss('css/bootsnav.css');
		$this->assets->addCss('css/style.css');
		$this->assets->addCss('css/responsive.css');
		$this->assets->addCss('css/fullcalendar.min.css');
		
		$this->assets->addJs('js/bootstrap.min.js');
		$this->assets->addJs('js/bootsnav.js');
		$this->assets->addJs('js/jquery.filterizr.min.js');
		$this->assets->addJs('js/jquery-ui.min.js');
		$this->assets->addJs('js/jquery.counterup.min.js');
		$this->assets->addJs('js/waypoints.min.js');
		$this->assets->addJs('js/owl.carousel.min.js');
		$this->assets->addJs('js/jquery.sticky.js');
		$this->assets->addJs('js/datepicker.js');
		$this->assets->addJs('js/custom.js');
		$this->assets->addJs('js/moment.min.js');
		$this->assets->addJs('js/fullcalendar.min.js');



        $ratingCollection = $this->assets->collection("rating");
        $ratingCollection->addJs('js/star-rating.min.js');
        $ratingCollection->addCss('css/fontawesome.min.css');
        $ratingCollection->addCss('css/star-rating.min.css');
	
		}	
	

}

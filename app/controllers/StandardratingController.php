<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Standardrating;

class StandardratingController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for standardrating
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Standardrating', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $standardrating = Standardrating::find($parameters);
        if (count($standardrating) == 0) {
            $this->flash->notice("The search did not find any standardrating");

            $this->dispatcher->forward([
                "controller" => "standardrating",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $standardrating,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction($standardpackageid)
    {
        $this->view->standardpackageid = $standardpackageid;

    }

    public function showRatingsAction($id)
    {
        $this->view->standardratings = standardrating::findBystandardpackageid($id);
    }

    /**
     * Edits a standardrating
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $standardrating = Standardrating::findFirstByid($id);
            if (!$standardrating) {
                $this->flash->error("standardrating was not found");

                $this->dispatcher->forward([
                    'controller' => "standardrating",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $standardrating->getId();

            $this->tag->setDefault("id", $standardrating->getId());
            $this->tag->setDefault("rating", $standardrating->getRating());
            $this->tag->setDefault("comment", $standardrating->getComment());
            $this->tag->setDefault("createdAt", $standardrating->getCreatedat());
            $this->tag->setDefault("standardpackageid", $standardrating->getStandardpackageid());
            $this->tag->setDefault("userid", $standardrating->getUserid());
            
        }
    }

    /**
     * Creates a new standardrating
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "standardrating",
                'action' => 'index'
            ]);

            return;
        }

        $standardrating = new Standardrating();
        $standardrating->setrating($this->request->getPost("rating"));
        $standardrating->setcomment($this->request->getPost("comment"));
        $standardrating->setcreatedAt((new DateTime())->format("Y-m-d H:i:s"));
        $standardrating->setstandardpackageid($this->request->getPost("standardpackageid"));
        $standardrating->setuserid($this->session->get("user")->getId());
        

        if (!$standardrating->save()) {
            foreach ($standardrating->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardrating",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("standardrating was created successfully");

        $this->dispatcher->forward([
            'controller' => "standardrating",
            'action' => 'displaygrid'
        ]);
    }

    /**
     * Saves a standardrating edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "standardrating",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $standardrating = Standardrating::findFirstByid($id);

        if (!$standardrating) {
            $this->flash->error("standardrating does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "standardrating",
                'action' => 'index'
            ]);

            return;
        }

        $standardrating->setrating($this->request->getPost("rating"));
        $standardrating->setcomment($this->request->getPost("comment"));
        $standardrating->setcreatedAt($this->request->getPost("createdAt"));
        $standardrating->setstandardpackageid($this->request->getPost("standardpackageid"));
        $standardrating->setuserid($this->request->getPost("userid"));
        

        if (!$standardrating->save()) {

            foreach ($standardrating->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardrating",
                'action' => 'edit',
                'params' => [$standardrating->getId()]
            ]);

            return;
        }

        $this->flash->success("standardrating was updated successfully");

        $this->dispatcher->forward([
            'controller' => "standardrating",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a standardrating
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $standardrating = Standardrating::findFirstByid($id);
        if (!$standardrating) {
            $this->flash->error("standardrating was not found");

            $this->dispatcher->forward([
                'controller' => "standardrating",
                'action' => 'index'
            ]);

            return;
        }

        if (!$standardrating->delete()) {

            foreach ($standardrating->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardrating",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("standardrating was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "standardrating",
            'action' => "index"
        ]);
    }

}

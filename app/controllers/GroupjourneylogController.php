<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Groupjourneylog;

class GroupjourneylogController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }
	
	public function displayCalendarAction($journeyid, $searchdate)
	{
		$fcCollection = $this->assets->collection("fullCalendar");
		$fcCollection->addJs('js/moment.min.js');
		$fcCollection->addJs('js/fullcalendar.min.js');
		$fcCollection->addCss('css/fullcalendar.min.css');
		$this->view->journeyid=$journeyid; 
		$this->view->searchdate=$searchdate;
	}
	
	public function jsonAction($journeyid)
	{
		//$this->view->disable();

		$journeyevents = Journeyevent::findByjourneyid($journeyid);
		$this->response->resetHeaders();
		$this->response->setContentType('application/json', 'UTF-8');
		$this->response->setContent(json_encode($journeyevents));
		return $this->response->send();
	}

    /**
     * Searches for groupjourneylog
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Groupjourneylog', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $groupjourneylog = Groupjourneylog::find($parameters);
        if (count($groupjourneylog) == 0) {
            $this->flash->notice("The search did not find any groupjourneylog");

            $this->dispatcher->forward([
                "controller" => "groupjourneylog",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $groupjourneylog,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a groupjourneylog
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $groupjourneylog = Groupjourneylog::findFirstByid($id);
            if (!$groupjourneylog) {
                $this->flash->error("groupjourneylog was not found");

                $this->dispatcher->forward([
                    'controller' => "groupjourneylog",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $groupjourneylog->getId();

            $this->tag->setDefault("id", $groupjourneylog->getId());
            $this->tag->setDefault("date", $groupjourneylog->getDate());
            $this->tag->setDefault("time", $groupjourneylog->getTime());
            $this->tag->setDefault("customergroupid", $groupjourneylog->getCustomergroupid());
            $this->tag->setDefault("journeyid", $groupjourneylog->getJourneyid());
            
        }
    }

    /**
     * Creates a new groupjourneylog
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "groupjourneylog",
                'action' => 'index'
            ]);

            return;
        }

        $groupjourneylog = new Groupjourneylog();
        $groupjourneylog->setdate($this->request->getPost("date"));
        $groupjourneylog->settime($this->request->getPost("time"));
        $groupjourneylog->setcustomergroupid($this->request->getPost("customergroupid"));
        $groupjourneylog->setjourneyid($this->request->getPost("journeyid"));
        

        if (!$groupjourneylog->save()) {
            foreach ($groupjourneylog->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "groupjourneylog",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("groupjourneylog was created successfully");

        $this->dispatcher->forward([
            'controller' => "groupjourneylog",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a groupjourneylog edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "groupjourneylog",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $groupjourneylog = Groupjourneylog::findFirstByid($id);

        if (!$groupjourneylog) {
            $this->flash->error("groupjourneylog does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "groupjourneylog",
                'action' => 'index'
            ]);

            return;
        }

        $groupjourneylog->setdate($this->request->getPost("date"));
        $groupjourneylog->settime($this->request->getPost("time"));
        $groupjourneylog->setcustomergroupid($this->request->getPost("customergroupid"));
        $groupjourneylog->setjourneyid($this->request->getPost("journeyid"));
        

        if (!$groupjourneylog->save()) {

            foreach ($groupjourneylog->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "groupjourneylog",
                'action' => 'edit',
                'params' => [$groupjourneylog->getId()]
            ]);

            return;
        }

        $this->flash->success("groupjourneylog was updated successfully");

        $this->dispatcher->forward([
            'controller' => "groupjourneylog",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a groupjourneylog
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $groupjourneylog = Groupjourneylog::findFirstByid($id);
        if (!$groupjourneylog) {
            $this->flash->error("groupjourneylog was not found");

            $this->dispatcher->forward([
                'controller' => "groupjourneylog",
                'action' => 'index'
            ]);

            return;
        }

        if (!$groupjourneylog->delete()) {

            foreach ($groupjourneylog->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "groupjourneylog",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("groupjourneylog was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "groupjourneylog",
            'action' => "index"
        ]);
    }

}

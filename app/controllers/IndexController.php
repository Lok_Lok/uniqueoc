<?php

class IndexController extends ControllerBase
{

    public function indexAction()
    {
		
    }

    public function LoggedInAction()
    {
        $layout = 'anonymous';

        if ($this->session->has('Registered Guest')) {
            $layout = 'index';
        }

        $this->view->setLayout($layout);
    }
}
?>
<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Payment;

class PaymentController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    public function successpaymentAction()
    {
    }

    /**
     * Searches for payment
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Payment', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $payment = Payment::find($parameters);
        if (count($payment) == 0) {
            $this->flash->notice("The search did not find any payment");

            $this->dispatcher->forward([
                "controller" => "payment",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $payment,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a payment
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $payment = Payment::findFirstByid($id);
            if (!$payment) {
                $this->flash->error("payment was not found");

                $this->dispatcher->forward([
                    'controller' => "payment",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $payment->getId();

            $this->tag->setDefault("id", $payment->getId());
            $this->tag->setDefault("paymentdate", $payment->getPaymentdate());
            $this->tag->setDefault("amount", $payment->getAmount());
            $this->tag->setDefault("userid", $payment->getUserid());
            
        }
    }

    /**
     * Creates a new payment
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "payment",
                'action' => 'index'
            ]);

            return;
        }

        $payment = new Payment();
        $payment->setpaymentdate((new DateTime())->format("Y-m-d H:i:s"));//will set to the current date/time
        $payment->setamount($this->request->getPost("amount"));
		$payment->setuserid($this->session->get('user')->getID());

    
        

        if (!$payment->save()) {
            foreach ($payment->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "payment",
                'action' => 'new'
            ]);

            return;
        }

       
		
			$this->dispatcher->forward([
                'controller' => "payment",
                'action' => 'successpayment'
            ]);
		$this->flash->success("Thank you for your payment. You paid €" .$payment->getAmount());
			
		 
    }

    /**
     * Saves a payment edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "payment",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $payment = Payment::findFirstByid($id);

        if (!$payment) {
            $this->flash->error("payment does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "payment",
                'action' => 'index'
            ]);

            return;
        }

        $payment->setpaymentdate($this->request->getPost("paymentdate"));
        $payment->setamount($this->request->getPost("amount"));
        $payment->setuserid($this->request->getPost("userid"));
        

        if (!$payment->save()) {

            foreach ($payment->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "payment",
                'action' => 'edit',
                'params' => [$payment->getId()]
            ]);

            return;
        }

        $this->flash->success("payment was updated successfully");

        $this->dispatcher->forward([
            'controller' => "payment",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a payment
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $payment = Payment::findFirstByid($id);
        if (!$payment) {
            $this->flash->error("payment was not found");

            $this->dispatcher->forward([
                'controller' => "payment",
                'action' => 'index'
            ]);

            return;
        }

        if (!$payment->delete()) {

            foreach ($payment->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "payment",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("payment was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "payment",
            'action' => "index"
        ]);
    }

}

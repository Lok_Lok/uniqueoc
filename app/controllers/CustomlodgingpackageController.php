<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Customlodgingpackage;

class CustomlodgingpackageController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for customlodgingpackage
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Customlodgingpackage', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $customlodgingpackage = Customlodgingpackage::find($parameters);
        if (count($customlodgingpackage) == 0) {
            $this->flash->notice("The search did not find any customlodgingpackage");

            $this->dispatcher->forward([
                "controller" => "customlodgingpackage",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $customlodgingpackage,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }
	
	public function successcreateAction()
    {

    }

    /**
     * Edits a customlodgingpackage
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $customlodgingpackage = Customlodgingpackage::findFirstByid($id);
            if (!$customlodgingpackage) {
                $this->flash->error("customlodgingpackage was not found");

                $this->dispatcher->forward([
                    'controller' => "customlodgingpackage",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $customlodgingpackage->getId();

            $this->tag->setDefault("id", $customlodgingpackage->getId());
            $this->tag->setDefault("lodginggroupbookingid", $customlodgingpackage->getLodginggroupbookingid());
            $this->tag->setDefault("userid", $customlodgingpackage->getUserid());
            
        }
    }

    /**
     * Creates a new customlodgingpackage
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "customlodgingpackage",
                'action' => 'index'
            ]);

            return;
        }


            $user = $this->session->get('user');
            if (!$user) {
                echo ("Please login to Proceed with this page!");

                $this->dispatcher->forward([
                    'controller' => "user",
                    'action' => 'index'
                ]);

                return;
            }
        $customlodgingpackage = new Customlodgingpackage();
        $customlodgingpackage->setlodginggroupbookingid($this->request->getPost("lodginggroupbookingid"));
        $customlodgingpackage->setuserid($this->session->get('user')->getID());
        

        if (!$customlodgingpackage->save()) {
            foreach ($customlodgingpackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customlodgingpackage",
                'action' => 'new'
            ]);

            return;
        }


        $this->dispatcher->forward([
            'controller' => "customlodgingpackage",
            'action' => 'successcreate'
        ]);
    }

    /**
     * Saves a customlodgingpackage edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "customlodgingpackage",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $customlodgingpackage = Customlodgingpackage::findFirstByid($id);

        if (!$customlodgingpackage) {
            $this->flash->error("customlodgingpackage does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "customlodgingpackage",
                'action' => 'index'
            ]);

            return;
        }

        $customlodgingpackage->setlodginggroupbookingid($this->request->getPost("lodginggroupbookingid"));
        $customlodgingpackage->setuserid($this->request->getPost("userid"));
        

        if (!$customlodgingpackage->save()) {

            foreach ($customlodgingpackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customlodgingpackage",
                'action' => 'edit',
                'params' => [$customlodgingpackage->getId()]
            ]);

            return;
        }

        $this->flash->success("customlodgingpackage was updated successfully");

        $this->dispatcher->forward([
            'controller' => "customlodgingpackage",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a customlodgingpackage
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $customlodgingpackage = Customlodgingpackage::findFirstByid($id);
        if (!$customlodgingpackage) {
            $this->flash->error("customlodgingpackage was not found");

            $this->dispatcher->forward([
                'controller' => "customlodgingpackage",
                'action' => 'index'
            ]);

            return;
        }

        if (!$customlodgingpackage->delete()) {

            foreach ($customlodgingpackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customlodgingpackage",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("customlodgingpackage was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "customlodgingpackage",
            'action' => "index"
        ]);
    }

}

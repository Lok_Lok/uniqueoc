<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Subscribe;

class SubscribeController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for subscribe
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Subscribe', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $subscribe = Subscribe::find($parameters);
        if (count($subscribe) == 0) {
            $this->flash->notice("The search did not find any subscribe");

            $this->dispatcher->forward([
                "controller" => "subscribe",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $subscribe,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    public function subscribesuccessAction()
    {

    }

    /**
     * Edits a subscribe
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $subscribe = Subscribe::findFirstByid($id);
            if (!$subscribe) {
                $this->flash->error("subscribe was not found");

                $this->dispatcher->forward([
                    'controller' => "subscribe",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $subscribe->getId();

            $this->tag->setDefault("id", $subscribe->getId());
            $this->tag->setDefault("email", $subscribe->getEmail());
            
        }
    }

    /**
     * Creates a new subscribe
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "subscribe",
                'action' => 'index'
            ]);

            return;
        }

        $subscribe = new Subscribe();
        $subscribe->setemail($this->request->getPost("email", "email"));
        

        if (!$subscribe->save()) {
            foreach ($subscribe->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "subscribe",
                'action' => 'new'
            ]);

            return;
        }


        $this->flash->success("subscribe was created successfully");

        $this->dispatcher->forward([
            'controller' => "subscribe",
            'action' => 'subscribesuccess'
        ]);
    }

    /**
     * Saves a subscribe edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "subscribe",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $subscribe = Subscribe::findFirstByid($id);

        if (!$subscribe) {
            $this->flash->error("subscribe does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "subscribe",
                'action' => 'index'
            ]);

            return;
        }

        $subscribe->setemail($this->request->getPost("email", "email"));
        

        if (!$subscribe->save()) {

            foreach ($subscribe->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "subscribe",
                'action' => 'edit',
                'params' => [$subscribe->getId()]
            ]);

            return;
        }

        $this->flash->success("subscribe was updated successfully");

        $this->dispatcher->forward([
            'controller' => "subscribe",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a subscribe
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $subscribe = Subscribe::findFirstByid($id);
        if (!$subscribe) {
            $this->flash->error("subscribe was not found");

            $this->dispatcher->forward([
                'controller' => "subscribe",
                'action' => 'index'
            ]);

            return;
        }

        if (!$subscribe->delete()) {

            foreach ($subscribe->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "subscribe",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("subscribe was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "subscribe",
            'action' => "index"
        ]);
    }

}

<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Guest;

class GuestController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for guest
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Guest', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $guest = Guest::find($parameters);
        if (count($guest) == 0) {
            $this->flash->notice("The search did not find any guest");

            $this->dispatcher->forward([
                "controller" => "guest",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $guest,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
	{
		$this->view->customergroups = occasion\Customergroup::find();
		$this->view->users = occasion\User::find();
	}

    /**
     * Edits a guest
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $guest = Guest::findFirstByid($id);
            if (!$guest) {
                $this->flash->error("guest was not found");

                $this->dispatcher->forward([
                    'controller' => "guest",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $guest->getId();

            $this->tag->setDefault("id", $guest->getId());
            $this->tag->setDefault("customergroupid", $guest->getCustomergroupid());
            $this->tag->setDefault("firstname", $guest->getFirstname());
            $this->tag->setDefault("surname", $guest->getSurname());
            $this->tag->setDefault("dateofbirth", $guest->getDateofbirth());
            $this->tag->setDefault("gender", $guest->getGender());
            $this->tag->setDefault("mobileno", $guest->getMobileno());
            $this->tag->setDefault("addressno", $guest->getAddressno());
            $this->tag->setDefault("streetname", $guest->getStreetname());
            $this->tag->setDefault("eircode", $guest->getEircode());
            $this->tag->setDefault("city", $guest->getCity());
            $this->tag->setDefault("county", $guest->getCounty());
            $this->tag->setDefault("email", $guest->getEmail());
            $this->tag->setDefault("password", $guest->getPassword());
            $this->tag->setDefault("userid", $guest->getUserid());
            
        }
    }

    /**
     * Creates a new guest
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "guest",
                'action' => 'index'
            ]);

            return;
        }

        $guest = new Guest();
        $guest->setcustomergroupid($this->request->getPost("customergroupid"));
        $guest->setfirstname($this->request->getPost("firstname"));
        $guest->setsurname($this->request->getPost("surname"));
        $guest->setdateofbirth($this->request->getPost("dateofbirth"));
        $guest->setgender($this->request->getPost("gender"));
        $guest->setmobileno($this->request->getPost("mobileno"));
        $guest->setaddressno($this->request->getPost("addressno"));
        $guest->setstreetname($this->request->getPost("streetname"));
        $guest->seteircode($this->request->getPost("eircode"));
        $guest->setcity($this->request->getPost("city"));
        $guest->setcounty($this->request->getPost("county"));
        $guest->setemail($this->request->getPost("email", "email"));
        $guest->setpassword($this->request->getPost("password"));
        $guest->setuserid($this->request->getPost("userid"));
        

        if (!$guest->save()) {
            foreach ($guest->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "guest",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("guest was created successfully");

        $this->dispatcher->forward([
            'controller' => "guest",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a guest edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "guest",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $guest = Guest::findFirstByid($id);

        if (!$guest) {
            $this->flash->error("guest does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "guest",
                'action' => 'index'
            ]);

            return;
        }

        $guest->setcustomergroupid($this->request->getPost("customergroupid"));
        $guest->setfirstname($this->request->getPost("firstname"));
        $guest->setsurname($this->request->getPost("surname"));
        $guest->setdateofbirth($this->request->getPost("dateofbirth"));
        $guest->setgender($this->request->getPost("gender"));
        $guest->setmobileno($this->request->getPost("mobileno"));
        $guest->setaddressno($this->request->getPost("addressno"));
        $guest->setstreetname($this->request->getPost("streetname"));
        $guest->seteircode($this->request->getPost("eircode"));
        $guest->setcity($this->request->getPost("city"));
        $guest->setcounty($this->request->getPost("county"));
        $guest->setemail($this->request->getPost("email", "email"));
        $guest->setpassword($this->request->getPost("password"));
        $guest->setuserid($this->request->getPost("userid"));
        

        if (!$guest->save()) {

            foreach ($guest->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "guest",
                'action' => 'edit',
                'params' => [$guest->getId()]
            ]);

            return;
        }

        $this->flash->success("guest was updated successfully");

        $this->dispatcher->forward([
            'controller' => "guest",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a guest
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $guest = Guest::findFirstByid($id);
        if (!$guest) {
            $this->flash->error("guest was not found");

            $this->dispatcher->forward([
                'controller' => "guest",
                'action' => 'index'
            ]);

            return;
        }

        if (!$guest->delete()) {

            foreach ($guest->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "guest",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("guest was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "guest",
            'action' => "index"
        ]);
    }

}

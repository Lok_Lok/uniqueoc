<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Custombooking;

class CustombookingController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for custombooking
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Custombooking', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $custombooking = Custombooking::find($parameters);
        if (count($custombooking) == 0) {
            $this->flash->notice("The search did not find any custombooking");

            $this->dispatcher->forward([
                "controller" => "custombooking",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $custombooking,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
     public function newAction()
	{
		$this->view->lodgingproviders = occasion\Lodgingprovider::find();
		$this->view->customergroups = occasion\Customergroup::find();
	}

    /**
     * Edits a custombooking
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $custombooking = Custombooking::findFirstByid($id);
            if (!$custombooking) {
                $this->flash->error("custombooking was not found");

                $this->dispatcher->forward([
                    'controller' => "custombooking",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $custombooking->getId();

            $this->tag->setDefault("id", $custombooking->getId());
            $this->tag->setDefault("lodgingproviderid", $custombooking->getLodgingproviderid());
            $this->tag->setDefault("customergroupid", $custombooking->getCustomergroupid());
            $this->tag->setDefault("cost", $custombooking->getCost());
            $this->tag->setDefault("paymenttype", $custombooking->getPaymenttype());
            $this->tag->setDefault("deposit", $custombooking->getDeposit());
            $this->tag->setDefault("balance", $custombooking->getBalance());
            
        }
    }

    /**
     * Creates a new custombooking
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "custombooking",
                'action' => 'index'
            ]);

            return;
        }

        $custombooking = new Custombooking();
        $custombooking->setlodgingproviderid($this->request->getPost("lodgingproviderid"));
        $custombooking->setcustomergroupid($this->request->getPost("customergroupid"));
        $custombooking->setcost($this->request->getPost("cost"));
        $custombooking->setpaymenttype($this->request->getPost("paymenttype"));
        $custombooking->setdeposit($this->request->getPost("deposit"));
        $custombooking->setbalance($this->request->getPost("balance"));
        

        if (!$custombooking->save()) {
            foreach ($custombooking->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "custombooking",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("custombooking was created successfully");

        $this->dispatcher->forward([
            'controller' => "custombooking",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a custombooking edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "custombooking",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $custombooking = Custombooking::findFirstByid($id);

        if (!$custombooking) {
            $this->flash->error("custombooking does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "custombooking",
                'action' => 'index'
            ]);

            return;
        }

        $custombooking->setlodgingproviderid($this->request->getPost("lodgingproviderid"));
        $custombooking->setcustomergroupid($this->request->getPost("customergroupid"));
        $custombooking->setcost($this->request->getPost("cost"));
        $custombooking->setpaymenttype($this->request->getPost("paymenttype"));
        $custombooking->setdeposit($this->request->getPost("deposit"));
        $custombooking->setbalance($this->request->getPost("balance"));
        

        if (!$custombooking->save()) {

            foreach ($custombooking->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "custombooking",
                'action' => 'edit',
                'params' => [$custombooking->getId()]
            ]);

            return;
        }

        $this->flash->success("custombooking was updated successfully");

        $this->dispatcher->forward([
            'controller' => "custombooking",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a custombooking
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $custombooking = Custombooking::findFirstByid($id);
        if (!$custombooking) {
            $this->flash->error("custombooking was not found");

            $this->dispatcher->forward([
                'controller' => "custombooking",
                'action' => 'index'
            ]);

            return;
        }

        if (!$custombooking->delete()) {

            foreach ($custombooking->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "custombooking",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("custombooking was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "custombooking",
            'action' => "index"
        ]);
    }

}

<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Standardpackagebooking;

class StandardpackagebookingController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }
	
	public function successcreateAction()
    {

    }

    /**
     * Searches for standardpackagebooking
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Standardpackagebooking', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $standardpackagebooking = Standardpackagebooking::find($parameters);
        if (count($standardpackagebooking) == 0) {
            $this->flash->notice("The search did not find any standardpackagebooking");

            $this->dispatcher->forward([
                "controller" => "standardpackagebooking",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $standardpackagebooking,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a standardpackagebooking
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $standardpackagebooking = Standardpackagebooking::findFirstByid($id);
            if (!$standardpackagebooking) {
                $this->flash->error("standardpackagebooking was not found");

                $this->dispatcher->forward([
                    'controller' => "standardpackagebooking",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $standardpackagebooking->getId();

            $this->tag->setDefault("id", $standardpackagebooking->getId());
            $this->tag->setDefault("standardpackagelogid", $standardpackagebooking->getStandardpackagelogid());
            $this->tag->setDefault("userid", $standardpackagebooking->getUserid());
            
        }
    }

    /**
     * Creates a new standardpackagebooking
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "standardpackagebooking",
                'action' => 'index'
            ]);
			return;
        }
            $user = $this->session->get('user');
            if (!$user) {
                echo ("Please login to Proceed with this page!");

                $this->dispatcher->forward([
                    'controller' => "user",
                    'action' => 'index'
                ]);

                return;
            }

        $standardpackagebooking = new Standardpackagebooking();
        $standardpackagebooking->setstandardpackagelogid($this->request->getPost("standardpackagelogid"));
        $standardpackagebooking->setuserid($this->session->get('user')->getID());
        

        if (!$standardpackagebooking->save()) {
            foreach ($standardpackagebooking->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardpackagebooking",
                'action' => 'new'
            ]);

            return;
        }

         $this->flash->success("Total cost of your Booking is : €" .$standardpackagebooking->getStandardpackagelog()->getStandardpackage()->getCost());
		return $this->dispatcher->forward(["controller" => "standardpackagebooking","action" => "successcreate"]);
    }
    /**
     * Saves a standardpackagebooking edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "standardpackagebooking",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $standardpackagebooking = Standardpackagebooking::findFirstByid($id);

        if (!$standardpackagebooking) {
            $this->flash->error("standardpackagebooking does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "standardpackagebooking",
                'action' => 'index'
            ]);

            return;
        }

        $standardpackagebooking->setstandardpackagelogid($this->request->getPost("standardpackagelogid"));
        $standardpackagebooking->setuserid($this->request->getPost("userid"));
        

        if (!$standardpackagebooking->save()) {

            foreach ($standardpackagebooking->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardpackagebooking",
                'action' => 'edit',
                'params' => [$standardpackagebooking->getId()]
            ]);

            return;
        }

        $this->flash->success("standardpackagebooking was updated successfully");

        $this->dispatcher->forward([
            'controller' => "standardpackagebooking",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a standardpackagebooking
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $standardpackagebooking = Standardpackagebooking::findFirstByid($id);
        if (!$standardpackagebooking) {
            $this->flash->error("standardpackagebooking was not found");

            $this->dispatcher->forward([
                'controller' => "standardpackagebooking",
                'action' => 'index'
            ]);

            return;
        }

        if (!$standardpackagebooking->delete()) {

            foreach ($standardpackagebooking->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "standardpackagebooking",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("standardpackagebooking was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "standardpackagebooking",
            'action' => "index"
        ]);
    }

}

<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Customergroup;

class CustomergroupController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for customergroup
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Customergroup', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $customergroup = Customergroup::find($parameters);
        if (count($customergroup) == 0) {
            $this->flash->notice("The search did not find any customergroup");

            $this->dispatcher->forward([
                "controller" => "customergroup",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $customergroup,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
	{
		$this->view->standardpackages = occasion\Standardpackage::find();
		$this->view->users = occasion\User::find();
	}

    /**
     * Edits a customergroup
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $customergroup = Customergroup::findFirstByid($id);
            if (!$customergroup) {
                $this->flash->error("customergroup was not found");

                $this->dispatcher->forward([
                    'controller' => "customergroup",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $customergroup->getId();

            $this->tag->setDefault("id", $customergroup->getId());
            $this->tag->setDefault("standardpackageid", $customergroup->getStandardpackageid());
            $this->tag->setDefault("leadguestname", $customergroup->getLeadguestname());
            $this->tag->setDefault("numberOfGuests", $customergroup->getNumberofguests());
            $this->tag->setDefault("userid", $customergroup->getUserid());
            
        }
    }

    /**
     * Creates a new customergroup
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "customergroup",
                'action' => 'index'
            ]);

            return;
        }

        $customergroup = new Customergroup();
        $customergroup->setstandardpackageid($this->request->getPost("standardpackageid"));
        $customergroup->setleadguestname($this->request->getPost("leadguestname"));
        $customergroup->setnumberOfGuests($this->request->getPost("numberOfGuests"));
		$customergroup->setuserid($this->session->get('user')->getID());
        

        if (!$customergroup->save()) {
            foreach ($customergroup->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customergroup",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("customergroup was created successfully");

        $this->dispatcher->forward([
            'controller' => "customergroup",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a customergroup edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "customergroup",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $customergroup = Customergroup::findFirstByid($id);

        if (!$customergroup) {
            $this->flash->error("customergroup does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "customergroup",
                'action' => 'index'
            ]);

            return;
        }

        $customergroup->setstandardpackageid($this->request->getPost("standardpackageid"));
        $customergroup->setleadguestname($this->request->getPost("leadguestname"));
        $customergroup->setnumberOfGuests($this->request->getPost("numberOfGuests"));
        $customergroup->setuserid($this->request->getPost("userid"));
        

        if (!$customergroup->save()) {

            foreach ($customergroup->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customergroup",
                'action' => 'edit',
                'params' => [$customergroup->getId()]
            ]);

            return;
        }

        $this->flash->success("customergroup was updated successfully");

        $this->dispatcher->forward([
            'controller' => "customergroup",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a customergroup
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $customergroup = Customergroup::findFirstByid($id);
        if (!$customergroup) {
            $this->flash->error("customergroup was not found");

            $this->dispatcher->forward([
                'controller' => "customergroup",
                'action' => 'index'
            ]);

            return;
        }

        if (!$customergroup->delete()) {

            foreach ($customergroup->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customergroup",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("customergroup was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "customergroup",
            'action' => "index"
        ]);
    }

}

<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Activityprovider;

class ActivityproviderController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

    /**
     * Searches for activityprovider
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Activityprovider', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $activityprovider = Activityprovider::find($parameters);
        if (count($activityprovider) == 0) {
            $this->flash->notice("The search did not find any activityprovider");

            $this->dispatcher->forward([
                "controller" => "activityprovider",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $activityprovider,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a activityprovider
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $activityprovider = Activityprovider::findFirstByid($id);
            if (!$activityprovider) {
                $this->flash->error("activityprovider was not found");

                $this->dispatcher->forward([
                    'controller' => "activityprovider",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $activityprovider->getId();

            $this->tag->setDefault("id", $activityprovider->getId());
            $this->tag->setDefault("companyname", $activityprovider->getCompanyname());
            $this->tag->setDefault("activityname", $activityprovider->getActivityname());
            $this->tag->setDefault("activitytype", $activityprovider->getActivitytype());
            $this->tag->setDefault("phoneno", $activityprovider->getPhoneno());
            $this->tag->setDefault("addressno", $activityprovider->getAddressno());
            $this->tag->setDefault("streetname", $activityprovider->getStreetname());
            $this->tag->setDefault("postalcode", $activityprovider->getPostalcode());
            $this->tag->setDefault("city", $activityprovider->getCity());
            $this->tag->setDefault("county", $activityprovider->getCounty());
            $this->tag->setDefault("email", $activityprovider->getEmail());
            $this->tag->setDefault("rating", $activityprovider->getRating());
            
        }
    }

    /**
     * Creates a new activityprovider
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "activityprovider",
                'action' => 'index'
            ]);

            return;
        }

        $activityprovider = new Activityprovider();
        $activityprovider->setcompanyname($this->request->getPost("companyname"));
        $activityprovider->setactivityname($this->request->getPost("activityname"));
        $activityprovider->setactivitytype($this->request->getPost("activitytype"));
        $activityprovider->setphoneno($this->request->getPost("phoneno"));
        $activityprovider->setaddressno($this->request->getPost("addressno"));
        $activityprovider->setstreetname($this->request->getPost("streetname"));
        $activityprovider->setpostalcode($this->request->getPost("postalcode"));
        $activityprovider->setcity($this->request->getPost("city"));
        $activityprovider->setcounty($this->request->getPost("county"));
        $activityprovider->setemail($this->request->getPost("email", "email"));
        $activityprovider->setrating($this->request->getPost("rating"));
        

        if (!$activityprovider->save()) {
            foreach ($activityprovider->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "activityprovider",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("activityprovider was created successfully");

        $this->dispatcher->forward([
            'controller' => "activityprovider",
            'action' => 'index'
        ]);
    }

    /**
     * Saves a activityprovider edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "activityprovider",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $activityprovider = Activityprovider::findFirstByid($id);

        if (!$activityprovider) {
            $this->flash->error("activityprovider does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "activityprovider",
                'action' => 'index'
            ]);

            return;
        }

        $activityprovider->setcompanyname($this->request->getPost("companyname"));
        $activityprovider->setactivityname($this->request->getPost("activityname"));
        $activityprovider->setactivitytype($this->request->getPost("activitytype"));
        $activityprovider->setphoneno($this->request->getPost("phoneno"));
        $activityprovider->setaddressno($this->request->getPost("addressno"));
        $activityprovider->setstreetname($this->request->getPost("streetname"));
        $activityprovider->setpostalcode($this->request->getPost("postalcode"));
        $activityprovider->setcity($this->request->getPost("city"));
        $activityprovider->setcounty($this->request->getPost("county"));
        $activityprovider->setemail($this->request->getPost("email", "email"));
        $activityprovider->setrating($this->request->getPost("rating"));
        

        if (!$activityprovider->save()) {

            foreach ($activityprovider->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "activityprovider",
                'action' => 'edit',
                'params' => [$activityprovider->getId()]
            ]);

            return;
        }

        $this->flash->success("activityprovider was updated successfully");

        $this->dispatcher->forward([
            'controller' => "activityprovider",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a activityprovider
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $activityprovider = Activityprovider::findFirstByid($id);
        if (!$activityprovider) {
            $this->flash->error("activityprovider was not found");

            $this->dispatcher->forward([
                'controller' => "activityprovider",
                'action' => 'index'
            ]);

            return;
        }

        if (!$activityprovider->delete()) {

            foreach ($activityprovider->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "activityprovider",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("activityprovider was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "activityprovider",
            'action' => "index"
        ]);
    }

}

<?php
 
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use occasion\Customjourneypackage;

class CustomjourneypackageController extends ControllerBase
{
    /**
     * Index action
     */
    public function indexAction()
    {
        $this->persistent->parameters = null;
    }

	public function displayCartAction()
	
	{
		
		$cust = $this->session->get('user');
		$custID = $cust->getID();
		$allBookings = Customjourneypackage::findByUserid($custID);
		$totalNoOfJournies = $allBookings->count();
		$this->view->myBookings=$allBookings;
		
	}

    /**
     * Searches for customjourneypackage
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, '\occasion\Customjourneypackage', $_POST);
            $this->persistent->parameters = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = $this->persistent->parameters;
        if (!is_array($parameters)) {
            $parameters = [];
        }
        $parameters["order"] = "id";

        $customjourneypackage = Customjourneypackage::find($parameters);
        if (count($customjourneypackage) == 0) {
            $this->flash->notice("The search did not find any customjourneypackage");

            $this->dispatcher->forward([
                "controller" => "customjourneypackage",
                "action" => "index"
            ]);

            return;
        }

        $paginator = new Paginator([
            'data' => $customjourneypackage,
            'limit'=> 10,
            'page' => $numberPage
        ]);

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Displays the creation form
     */
    public function newAction()
    {

    }

    /**
     * Edits a customjourneypackage
     *
     * @param string $id
     */
    public function editAction($id)
    {
        if (!$this->request->isPost()) {

            $customjourneypackage = Customjourneypackage::findFirstByid($id);
            if (!$customjourneypackage) {
                $this->flash->error("customjourneypackage was not found");

                $this->dispatcher->forward([
                    'controller' => "customjourneypackage",
                    'action' => 'index'
                ]);

                return;
            }

            $this->view->id = $customjourneypackage->getId();

            $this->tag->setDefault("id", $customjourneypackage->getId());
            $this->tag->setDefault("groupjourneylogid", $customjourneypackage->getGroupjourneylogid());
            $this->tag->setDefault("userid", $customjourneypackage->getUserid());
            
        }
    }

    /**
     * Creates a new customjourneypackage
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "customjourneypackage",
                'action' => 'index'
            ]);

            return;
        }

            $user = $this->session->get('user');
            if (!$user) {
                echo ("Please login to Proceed with this page!");

                $this->dispatcher->forward([
                    'controller' => "user",
                    'action' => 'index'
                ]);

                return;
            }
        $customjourneypackage = new Customjourneypackage();
        $customjourneypackage->setgroupjourneylogid($this->request->getPost("groupjourneylogid"));
        $customjourneypackage->setuserid($this->session->get('user')->getID());
        

        if (!$customjourneypackage->save()) {
            foreach ($customjourneypackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customjourneypackage",
                'action' => 'new'
            ]);

            return;
        }

        $this->flash->success("Total cost of your journey is : €" .$customjourneypackage->getGroupjourneylog()->getJourney()->getCost());
		return $this->dispatcher->forward(["controller" => "customactivitypackage","action" => "successcreate"]);
    }

    /**
     * Saves a customjourneypackage edited
     *
     */
    public function saveAction()
    {

        if (!$this->request->isPost()) {
            $this->dispatcher->forward([
                'controller' => "customjourneypackage",
                'action' => 'index'
            ]);

            return;
        }

        $id = $this->request->getPost("id");
        $customjourneypackage = Customjourneypackage::findFirstByid($id);

        if (!$customjourneypackage) {
            $this->flash->error("customjourneypackage does not exist " . $id);

            $this->dispatcher->forward([
                'controller' => "customjourneypackage",
                'action' => 'index'
            ]);

            return;
        }

        $customjourneypackage->setgroupjourneylogid($this->request->getPost("groupjourneylogid"));
        $customjourneypackage->setuserid($this->request->getPost("userid"));
        

        if (!$customjourneypackage->save()) {

            foreach ($customjourneypackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customjourneypackage",
                'action' => 'edit',
                'params' => [$customjourneypackage->getId()]
            ]);

            return;
        }

        $this->flash->success("customjourneypackage was updated successfully");

        $this->dispatcher->forward([
            'controller' => "customjourneypackage",
            'action' => 'index'
        ]);
    }

    /**
     * Deletes a customjourneypackage
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $customjourneypackage = Customjourneypackage::findFirstByid($id);
        if (!$customjourneypackage) {
            $this->flash->error("customjourneypackage was not found");

            $this->dispatcher->forward([
                'controller' => "customjourneypackage",
                'action' => 'index'
            ]);

            return;
        }

        if (!$customjourneypackage->delete()) {

            foreach ($customjourneypackage->getMessages() as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'controller' => "customjourneypackage",
                'action' => 'search'
            ]);

            return;
        }

        $this->flash->success("customjourneypackage was deleted successfully");

        $this->dispatcher->forward([
            'controller' => "customjourneypackage",
            'action' => "index"
        ]);
    }

}

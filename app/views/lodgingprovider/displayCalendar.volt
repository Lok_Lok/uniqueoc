{{ content()}}
{{ assets.outputCss('fullCalendar') }}
{{ assets.outputJs('fullCalendar') }}
<script>
$(document).ready(function() {
    $('#calendar').fullCalendar({
        header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
        defaultDate: '2019-08-03',
        editable: true,
        eventLimit: true,
        events: [
        {
          title: 'All Day Event',
          start: '2019-08-03'
        }]
    });
});
</script>
<div id="calendar"></div>
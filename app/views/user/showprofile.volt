<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>
<?php
$this->assets->outputCss();
$this->assets->outputJs();
?>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
<script src="https://cdnjs.cloudflare.com/ajax/libs/zxcvbn/4.2.0/zxcvbn.js"></script>

<?php $user = $this->session->get('user');?>

<br>
<br>
<br>

<div class="page-header">
    <h1>User Profile</h1>
</div>

<?php echo $this->getContent(); ?>

<?php
    echo $this->tag->form(
        [
            "user/save",
            "autocomplete" => "off",
            "class" => "form-horizontal"
        ]
    );
?>

<div class="form-group">
    <label for="fieldUsername" class="col-sm-2 control-label">User Name</label>
    <div class="col-sm-10">
        <?php echo $this->tag->telField(["username", "size" => 30, "class" => "form-control", "id" => "fieldUsername", "readonly" => "readonly"]) ?>
    </div>
</div>



<div class="form-group">
    <label for="fieldFirstname" class="col-sm-2 control-label">First Name</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["firstname", "size" => 30, "class" => "form-control", "id" => "fieldFirstname", "readonly" => "readonly"]) ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldSurname" class="col-sm-2 control-label">Sur Name</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["surname", "size" => 30, "class" => "form-control", "id" => "fieldSurname", "readonly" => "readonly"]) ?>
    </div>
</div>

<div class="form-group">
    <label for="fieldEmailaddress" class="col-sm-2 control-label">Email Address</label>
    <div class="col-sm-10">
        <?php echo $this->tag->textField(["emailAddress", "size" => 30, "class" => "form-control", "id" => "fieldEmailaddress", "readonly" => "readonly"]) ?>
    </div>
</div>







<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <button class ="btn btn-default"><?php echo $this->tag->linkTo(["user/edit/" . $user->getId(), "Edit Details"]); ?></button>
    </div>
</div>

<?php echo $this->tag->endForm(); ?>

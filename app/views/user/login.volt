<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>
<?php
$this->assets->outputCss();
$this->assets->outputJs();
?>

<style>

body {
  background-image: url("../img/abc2.jpg");
  background-repeat: no-repeat
	}
	h3{
		color: white;
        font-size: 35px;

	}
	h2{
    		color: white;
    		font-size: 15px;
    }
	h4{
		background: white;
		width:250px;
	}
</style>
<body>

<div class="page-header" style="margin-top:80px;">
	<h3 align = "center"><img src="../img/logotransparent.png" alt="logo"></h3><br>
	<h3 align = "center">User Log In</h3>
	<br>
	<br>
</div>



<?php echo $this->getContent() ?>


{{ form('user/authorize', 'role': 'form') }}
	<fieldset>
		<div class= "col-sm-3"></div>
		<div class= "col-sm-6">
			<div class="form-group1 bg-blue">
				<h2>Enter User Name</h2>
				<div class="controls">
					{{ text_field('username', 'class': "form-control") }}
				</div>
			</div>
			<div class="form-group">
				<h2>Enter Password</h2>
				<div class="controls">
					{{ password_field('password', 'class': "form-control") }}
			</div>
			</div>

			<div class="form-group">
                {{ submit_button('Login', 'class': 'btn btn-info btn-block  my-4') }}
            </div>

            <div class="form-group">
                 <div class ="'btn btn-info btn-block  my-4" align ="center" ><?php echo $this->tag->linkTo(["user/new" , "No Account? Sign up here!"]); ?></div>
            </div>





		</div>


	</fieldset>
{{  endform() }}

</body>
<br>

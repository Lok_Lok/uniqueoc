<?= $this->getContent() ?>
<?= $this->assets->outputCss();?>
<?= $this->assets->outputCss('fullCalendar') ?>
<?= $this->assets->outputJs('fullCalendar') ?>

<div class="container">
  <div class="page-header" style="margin-top:80px;">
    <h2>Please Choose a date</h2>      
  </div> 
  
<script>
$(document).ready(function() {
    $('#calendar').fullCalendar({
		aspectRatio:4,
        header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek',
            },
        defaultDate: '{{searchdate}}',
		navLinks: true,
        editable: true,
		slotDuration: '00:10:00',
        eventLimit: true,
		eventClick: function(event) {
			$('#timeofcheckin').val(event.start.format('hh:mm'));
			$('#datecheckin').val(event.start.format('YYYY-MM-DD'));
			$('#datecheckout').val(event.end.format('YYYY-MM-DD'));
			$('#lodgingtypeid').val(event.lodgingtypeid);
			$('#lodginggroupbookingid').val(event.id);
			$('#cost').val(event.cost);
			$('#numberOfbeds').val(event.noofbeds);
			$('#numberOfrooms').val(event.noofrooms);
			$('#title').val(event.title);
			$('#lgb').val(event.lodginggroupbooking);
			$('#lodgingprovidername').val(event.title);
			$('#fullCalModal').modal('show');
		},

		events: "{{ url('lodginggroupbooking/json/'~lodgingtypeid) }}",
		
		
    });
});
$(function () {
		$('body').on('click', '#submitButton', function (e) {
		$(this.form).submit();
		$('#fullCalModal').modal('hide');
	});
});

</script>


<div id="calendar"></div>
<div id="fullCalModal" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Lodging Booking</h4>
      </div>
      <div class="modal-body">
            <div class="container-fluid">
			<form action="{{url('customlodgingpackage/create')}}" method="post">

					<div class="hidden">
                        <label hidden for="lodginggroupbookingid">Lodging ID</label>
                        <input type="text" class="form-control" id="lodginggroupbookingid" name="lodginggroupbookingid"/>
                    </div>
					
					<div class="hidden">
                        <label hidden for="lodgingtypeid">Lodging type ID</label>
                        <input type="text" class="form-control" id="lodgingtypeid" name="lodgingtypeid"/>
                    </div>
					
					<div class="form-group">
                        <label for="cost">Cost €</label>
                        <input type="text" class="form-control" id="cost" name="cost"/>
                    </div>
					
					<div class="form-group">
                        <label for="numberOfrooms">Number of Rooms</label>
                        <input type="text" class="form-control" id="numberOfrooms" name="numberOfrooms"/>
                    </div>
					
					<div class="form-group">
                        <label for="numberOfbeds">Number of Beds</label>
                        <input type="text" class="form-control" id="numberOfbeds" name="numberOfbeds"/>
                    </div>
					
					<div class="form-group">
                        <label for="datecheckin">Check-in</label>
                        <input type="text" class="form-control" id="datecheckin" name="datecheckin"/>

                    </div>
					
					<div class="form-group">
                        <label for="datecheckout">Checkout</label>
                        <input type="text" class="form-control" id="datecheckout" name="datecheckout"/>

                    </div>

                        <label for="title">Name</label>
                        <input type="text" class="form-control" id="title" name="title"/>
                    </div>
					

                    <div class="modal-footer">
						<button id="addItem" type="submit"  class="btn btn-success center-block addItem" value="<?php echo $lodginggroupbooking;?>">Book now</button>
                    </div>
					

                </form>
            </div>
       </div> 
    </div>
  </div>
</div>
<?php
/**
 * @var \Phalcon\Mvc\View\Engine\Php $this
 */
?>

<?php
$this->assets->outputCss();
$this->assets->outputJs();
?>

<style>

body {
  background-color: #ffffff
;
  background-repeat: no-repeat
	}
	h3{
		color: black;
        font-size: 35px;

	}
	h2{
    		color: black;
    		font-size: 15px;
    }
	h4{
		background: black;
		width:250px;
	}
</style>
<body>
{{ content() }}
<div class="page-header">
	<h3 align = "center"><img src="../img/logotransparent.png" alt="logo"></h3><br>
	<h3 align = "center">Administrator Log In</h3>

</div>
{{ form('administrator/authorize', 'role': 'form') }}
	<fieldset>
	<div class= "col-sm-3"></div>
    <div class= "col-sm-6">
		<div class="form-group1 bg-blue">
			<label for="email">Email</label>
			<div class="controls">
				{{ text_field('email', 'class': "form-control") }}
			</div>
		</div>
		<div class="form-group">
			<label for="password">Password</label>
			<div class="controls">
				{{ password_field('password', 'class': "form-control") }}
			</div>
		</div>
		<div class="form-group">
					{{ submit_button('Login', 'class': 'btn btn-info btn-block  my-4') }}
		</div>

		<br>

	</fieldset>
{{  endform() }}
</body>
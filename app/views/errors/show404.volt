{{ content() }}
<?php
	$this->assets->outputCss();
	$this->assets->outputJs();
?>

<div class="page-header" style="margin-top:80px;">
<div class="jumbotron">
    <h1>Page not found</h1>
    <p>Sorry, you have accessed a page that does not exist or was moved</p>
    <p>{{ link_to('index', 'Return to Homepage', 'class': 'btn btn-primary') }}</p>
</div>
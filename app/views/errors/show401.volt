{{ content() }}
<?php
	$this->assets->outputCss();
	$this->assets->outputJs();
?>

<div class="page-header" style="margin-top:80px;">
<div class="jumbotron">
    <h1>Not authorized</h1>
    <p>You are not authorized to view this page</p>
    <p>{{ link_to('index', 'Return to Homepage', 'class': 'btn btn-primary') }}</p>
</div>
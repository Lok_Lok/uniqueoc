{{ content() }}
<?php
	$this->assets->outputCss();
	$this->assets->outputJs();
?>

<div class="page-header" style="margin-top:80px;">
<div class="jumbotron">
    <h1>Internal Error</h1>
    <p>Something went wrong, please contact the website administrator</p>
    <p>{{ link_to('index', 'Return to Homepage', 'class': 'btn btn-primary') }}</p>
</div>
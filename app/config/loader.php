<?php

$loader = new \Phalcon\Loader();
$loader->registerNamespaces(
    [
        "occasion"    => $config->application->modelsDir,
        "security"    => $config->application->modelsDir

    ]
);
/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
		$config->application->pluginsDir
    ]
)->register();
require_once __DIR__ . "/../../vendor/autoload.php";
<?php

namespace occasion;

class Standardpackagelog extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $scheduleactivitylogid;

    /**
     *
     * @var integer
     */
    protected $standardpackageid;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field scheduleactivitylogid
     *
     * @param integer $scheduleactivitylogid
     * @return $this
     */
    public function setScheduleactivitylogid($scheduleactivitylogid)
    {
        $this->scheduleactivitylogid = $scheduleactivitylogid;

        return $this;
    }

    /**
     * Method to set the value of field standardpackageid
     *
     * @param integer $standardpackageid
     * @return $this
     */
    public function setStandardpackageid($standardpackageid)
    {
        $this->standardpackageid = $standardpackageid;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field scheduleactivitylogid
     *
     * @return integer
     */
    public function getScheduleactivitylogid()
    {
        return $this->scheduleactivitylogid;
    }

    /**
     * Returns the value of field standardpackageid
     *
     * @return integer
     */
    public function getStandardpackageid()
    {
        return $this->standardpackageid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("standardpackagelog");
        $this->belongsTo('standardpackageid', 'occasion\Standardpackage', 'id', ['alias' => 'Standardpackage']);
        $this->belongsTo('scheduleactivitylogid', 'occasion\Scheduledactivity', 'id', ['alias' => 'Scheduledactivity']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'standardpackagelog';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Standardpackagelog[]|Standardpackagelog|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Standardpackagelog|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

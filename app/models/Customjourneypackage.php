<?php

namespace occasion;

class Customjourneypackage extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $groupjourneylogid;

    /**
     *
     * @var integer
     */
    protected $userid;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field groupjourneylogid
     *
     * @param integer $groupjourneylogid
     * @return $this
     */
    public function setGroupjourneylogid($groupjourneylogid)
    {
        $this->groupjourneylogid = $groupjourneylogid;

        return $this;
    }

    /**
     * Method to set the value of field userid
     *
     * @param integer $userid
     * @return $this
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field groupjourneylogid
     *
     * @return integer
     */
    public function getGroupjourneylogid()
    {
        return $this->groupjourneylogid;
    }

    /**
     * Returns the value of field userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("customjourneypackage");
        $this->belongsTo('groupjourneylogid', 'occasion\Groupjourneylog', 'id', ['alias' => 'Groupjourneylog']);
        $this->belongsTo('userid', 'occasion\User', 'id', ['alias' => 'User']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customjourneypackage';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customjourneypackage[]|Customjourneypackage|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customjourneypackage|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

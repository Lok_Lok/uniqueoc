<?php

namespace occasion;

class Activitytype extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $product;

    /**
     *
     * @var string
     */
    protected $activitylevel;

    /**
     *
     * @var string
     */
    protected $activitybase;

    /**
     *
     * @var integer
     */
    protected $cost;

    /**
     *
     * @var string
     */
    protected $insurance;

    /**
     *
     * @var string
     */
    protected $description;

    /**
     *
     * @var string
     */
    protected $activitypic;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field product
     *
     * @param string $product
     * @return $this
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Method to set the value of field activitylevel
     *
     * @param string $activitylevel
     * @return $this
     */
    public function setActivitylevel($activitylevel)
    {
        $this->activitylevel = $activitylevel;

        return $this;
    }

    /**
     * Method to set the value of field activitybase
     *
     * @param string $activitybase
     * @return $this
     */
    public function setActivitybase($activitybase)
    {
        $this->activitybase = $activitybase;

        return $this;
    }

    /**
     * Method to set the value of field cost
     *
     * @param integer $cost
     * @return $this
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Method to set the value of field insurance
     *
     * @param string $insurance
     * @return $this
     */
    public function setInsurance($insurance)
    {
        $this->insurance = $insurance;

        return $this;
    }

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Method to set the value of field activitypic
     *
     * @param string $activitypic
     * @return $this
     */
    public function setActivitypic($activitypic)
    {
        $this->activitypic = $activitypic;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field product
     *
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Returns the value of field activitylevel
     *
     * @return string
     */
    public function getActivitylevel()
    {
        return $this->activitylevel;
    }

    /**
     * Returns the value of field activitybase
     *
     * @return string
     */
    public function getActivitybase()
    {
        return $this->activitybase;
    }

    /**
     * Returns the value of field cost
     *
     * @return integer
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Returns the value of field insurance
     *
     * @return string
     */
    public function getInsurance()
    {
        return $this->insurance;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Returns the value of field activitypic
     *
     * @return string
     */
    public function getActivitypic()
    {
        return $this->activitypic;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("activitytype");
        $this->hasMany('id', 'occasion\Activityrating', 'activitytypeid', ['alias' => 'Activityrating']);
        $this->hasMany('id', 'occasion\Scheduledactivity', 'activitytypeid', ['alias' => 'Scheduledactivity']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'activitytype';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Activitytype[]|Activitytype|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Activitytype|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

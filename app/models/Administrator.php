<?php

namespace occasion;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class Administrator extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $firstname;

    /**
     *
     * @var string
     */
    protected $surname;

    /**
     *
     * @var string
     */
    protected $dob;

    /**
     *
     * @var string
     */
    protected $mobileno;

    /**
     *
     * @var integer
     */
    protected $addressno;

    /**
     *
     * @var string
     */
    protected $streetname;

    /**
     *
     * @var string
     */
    protected $eircode;

    /**
     *
     * @var string
     */
    protected $city;

    /**
     *
     * @var string
     */
    protected $county;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var string
     */
    protected $department;

    /**
     *
     * @var string
     */
    protected $reportsto;

    /**
     *
     * @var string
     */
    protected $salary;

    /**
     *
     * @var string
     */
    protected $ppsn;

    /**
     *
     * @var string
     */
    protected $status;

    /**
     *
     * @var string
     */
    protected $role;

    /**
     *
     * @var string
     */
    protected $validationkey;

    /**
     *
     * @var string
     */
    protected $createdat;

	/**
     *
     * @var string
     * @Column(column="updatedat", type="string", nullable=false)
     */
    protected $updatedat;
    
	/**
     *
     * @var string
     */
    protected $password;
	

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field firstname
     *
     * @param string $firstname
     * @return $this
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Method to set the value of field surname
     *
     * @param string $surname
     * @return $this
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Method to set the value of field dob
     *
     * @param string $dob
     * @return $this
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Method to set the value of field mobileno
     *
     * @param string $mobileno
     * @return $this
     */
    public function setMobileno($mobileno)
    {
        $this->mobileno = $mobileno;

        return $this;
    }

    /**
     * Method to set the value of field addressno
     *
     * @param integer $addressno
     * @return $this
     */
    public function setAddressno($addressno)
    {
        $this->addressno = $addressno;

        return $this;
    }

    /**
     * Method to set the value of field streetname
     *
     * @param string $streetname
     * @return $this
     */
    public function setStreetname($streetname)
    {
        $this->streetname = $streetname;

        return $this;
    }

    /**
     * Method to set the value of field eircode
     *
     * @param string $eircode
     * @return $this
     */
    public function setEircode($eircode)
    {
        $this->eircode = $eircode;

        return $this;
    }

    /**
     * Method to set the value of field city
     *
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Method to set the value of field county
     *
     * @param string $county
     * @return $this
     */
    public function setCounty($county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field department
     *
     * @param string $department
     * @return $this
     */
    public function setDepartment($department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Method to set the value of field reportsto
     *
     * @param string $reportsto
     * @return $this
     */
    public function setReportsto($reportsto)
    {
        $this->reportsto = $reportsto;

        return $this;
    }

    /**
     * Method to set the value of field salary
     *
     * @param string $salary
     * @return $this
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Method to set the value of field ppsn
     *
     * @param string $ppsn
     * @return $this
     */
    public function setPpsn($ppsn)
    {
        $this->ppsn = $ppsn;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field role
     *
     * @param string $role
     * @return $this
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Method to set the value of field validationkey
     *
     * @param string $validationkey
     * @return $this
     */
    public function setValidationkey($validationkey)
    {
        $this->validationkey = $validationkey;

        return $this;
    }

    /**
     * Method to set the value of field createdat
     *
     * @param string $createdat
     * @return $this
     */
    public function setCreatedat($createdat)
    {
        $this->createdat = $createdat;

        return $this;
    }

	/**
     * Method to set the value of field updatedat
     *
     * @param string $updatedat
     * @return $this
     */
    public function setUpdatedat($updatedat)
    {
        $this->updatedat = $updatedat;

        return $this;
    }
	
    /**
     * Method to set the value of field password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Returns the value of field surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Returns the value of field dob
     *
     * @return string
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * Returns the value of field mobileno
     *
     * @return string
     */
    public function getMobileno()
    {
        return $this->mobileno;
    }

    /**
     * Returns the value of field addressno
     *
     * @return integer
     */
    public function getAddressno()
    {
        return $this->addressno;
    }

    /**
     * Returns the value of field streetname
     *
     * @return string
     */
    public function getStreetname()
    {
        return $this->streetname;
    }

    /**
     * Returns the value of field eircode
     *
     * @return string
     */
    public function getEircode()
    {
        return $this->eircode;
    }

    /**
     * Returns the value of field city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Returns the value of field county
     *
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field department
     *
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Returns the value of field reportsto
     *
     * @return string
     */
    public function getReportsto()
    {
        return $this->reportsto;
    }

    /**
     * Returns the value of field salary
     *
     * @return string
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * Returns the value of field ppsn
     *
     * @return string
     */
    public function getPpsn()
    {
        return $this->ppsn;
    }

    /**
     * Returns the value of field status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Returns the value of field role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Returns the value of field validationkey
     *
     * @return string
     */
    public function getValidationkey()
    {
        return $this->validationkey;
    }

    /**
     * Returns the value of field createdat
     *
     * @return string
     */
    public function getCreatedat()
    {
        return $this->createdat;
    }

	/**
     * Returns the value of field updatedat
     *
     * @return string
     */
    public function getUpdatedat()
    {
        return $this->updatedat;
    }
	
    /**
     * Returns the value of field password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
	
	public function __toString()
	{
		return $this->firstname . " " . $this->surname;
	}

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $validator = new Validation();

        /*$validator->add(
            "dateofbirth",
            new dateValidator(
                [
                    "format"  => "d-m-y",
                    "message" => "The date is invalid",
                ]
            )
        );*/

        /*$validator->add(
            [
                "dateofbirth",
                "anotherdateofbirth",
            ],
            new dateValidator(
                [
                    "format" => [
                        "date"        => "d-m-Y",
                        "anotherdate" => "Y-m-d",
                    ],
                    "message" => [
                        "dateofbirth"        => "The dateofbirth is invalid",
                        "anotherdateofbirth" => "The another dateofbirth is invalid",
                    ],
                ]
            )
        );*/

        $validator = new Validation();


        $validator->add(
            "email",
            new emailValidator(
                [
                    "message" => "The e-mail is not valid",
                ]
            )
        );

        $validator->add(
            [
                "email",
                "anotheremail",
            ],
            new emailValidator(
                [
                    "message" => [
                        "email"        => "The e-mail is not valid",
                        "anotheremail" => "The another e-mail is not valid",
                    ],
                ]
            )
        );


        $validator= new Validation();
        $uValidator = new UniquenessValidator(["message" => "this email has already been chosen"]);
        $validator->add('email', $uValidator);
        return $this->validate($validator);


    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("administrator");
        $this->hasMany('id', 'occasion\Journey', 'administratorid', ['alias' => 'Journey']);
        $this->hasMany('id', 'occasion\Specialrequest', 'administratorid', ['alias' => 'Specialrequest']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'administrator';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Administrator[]|Administrator|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Administrator|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

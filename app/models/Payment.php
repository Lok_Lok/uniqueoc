<?php

namespace occasion;




class Payment extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $paymentdate;

    /**
     *
     * @var integer
     */
    protected $amount;
	
	

    /**
     *
     * @var integer
     */
    protected $userid;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field paymentdate
     *
     * @param string $paymentdate
     * @return $this
     */
    public function setPaymentdate($paymentdate)
    {
        $this->paymentdate = $paymentdate;

        return $this;
    }

    /**
     * Method to set the value of field amount
     *
     * @param integer $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Method to set the value of field userid
     *
     * @param integer $userid
     * @return $this
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }
	
	
	
	

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field paymentdate
     *
     * @return string
     */
    public function getPaymentdate()
    {
        return $this->paymentdate;
    }

    /**
     * Returns the value of field amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Returns the value of field userid
     *
     * @return integer
     */
    public function getUserid()
	{
		return $this->userid;
	}
	
	

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("payment");
        $this->belongsTo('userid', 'occasion\User', 'id', ['alias' => 'User']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'payment';
    }

	
    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Payment[]|Payment|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Payment|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }
	
	/**public function validation()
	{
	   $validator= new Validation();
	   //create validator
	   $poValidator = new PresenceOfValidator();
	   
	   //add the validator to the various fields which need them
	   $validator->add('Full name',$poValidator); //adds presence of validator to firstname field
	   $validator->add('Card number',$poValidator); //adds presence of validator to card number field
	   
	   //the validation function must return a validator object
	   return $this->validate($validator);
	
	}*/

}

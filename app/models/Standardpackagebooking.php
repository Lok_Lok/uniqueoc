<?php

namespace occasion;

class Standardpackagebooking extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var integer
     * @Column(column="standardpackagelogid", type="integer", length=8, nullable=false)
     */
    protected $standardpackagelogid;

    /**
     *
     * @var integer
     * @Column(column="userid", type="integer", length=11, nullable=false)
     */
    protected $userid;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field standardpackagelogid
     *
     * @param integer $standardpackagelogid
     * @return $this
     */
    public function setStandardpackagelogid($standardpackagelogid)
    {
        $this->standardpackagelogid = $standardpackagelogid;

        return $this;
    }

    /**
     * Method to set the value of field userid
     *
     * @param integer $userid
     * @return $this
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field standardpackagelogid
     *
     * @return integer
     */
    public function getStandardpackagelogid()
    {
        return $this->standardpackagelogid;
    }

    /**
     * Returns the value of field userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("standardpackagebooking");
        $this->belongsTo('standardpackagelogid', 'occasion\Standardpackagelog', 'id', ['alias' => 'Standardpackagelog']);
        $this->belongsTo('userid', 'occasion\User', 'id', ['alias' => 'User']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'standardpackagebooking';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Standardpackagebooking[]|Standardpackagebooking|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Standardpackagebooking|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

<?php

namespace occasion;

class Lodginggroupbooking extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=8, nullable=false)
     */
    protected $id;

    /**
     *
     * @var integer
     * @Column(column="standardpackageid", type="integer", length=9, nullable=true)
     */
    protected $standardpackageid;

    /**
     *
     * @var integer
     * @Column(column="lodgingtypeid", type="integer", length=11, nullable=false)
     */
    protected $lodgingtypeid;

    /**
     *
     * @var string
     * @Column(column="timeofcheckin", type="string", nullable=true)
     */
    protected $timeofcheckin;

    /**
     *
     * @var string
     * @Column(column="timeofcheckout", type="string", nullable=true)
     */
    protected $timeofcheckout;

    /**
     *
     * @var integer
     * @Column(column="noofrooms", type="integer", length=9, nullable=true)
     */
    protected $noofrooms;

    /**
     *
     * @var string
     * @Column(column="roomtype", type="string", length=30, nullable=true)
     */
    protected $roomtype;

    /**
     *
     * @var integer
     * @Column(column="noofbeds", type="integer", length=9, nullable=true)
     */
    protected $noofbeds;

    /**
     *
     * @var string
     * @Column(column="cost", type="string", length=40, nullable=true)
     */
    protected $cost;

    /**
     *
     * @var string
     * @Column(column="deposit", type="string", length=40, nullable=true)
     */
    protected $deposit;

    /**
     *
     * @var string
     * @Column(column="balance", type="string", length=40, nullable=true)
     */
    protected $balance;

    /**
     *
     * @var string
     * @Column(column="datecheckin", type="string", nullable=true)
     */
    protected $datecheckin;

    /**
     *
     * @var string
     * @Column(column="datecheckout", type="string", nullable=true)
     */
    protected $datecheckout;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field standardpackageid
     *
     * @param integer $standardpackageid
     * @return $this
     */
    public function setStandardpackageid($standardpackageid)
    {
        $this->standardpackageid = $standardpackageid;

        return $this;
    }

    /**
     * Method to set the value of field lodgingtypeid
     *
     * @param integer $lodgingtypeid
     * @return $this
     */
    public function setLodgingtypeid($lodgingtypeid)
    {
        $this->lodgingtypeid = $lodgingtypeid;

        return $this;
    }

    /**
     * Method to set the value of field timeofcheckin
     *
     * @param string $timeofcheckin
     * @return $this
     */
    public function setTimeofcheckin($timeofcheckin)
    {
        $this->timeofcheckin = $timeofcheckin;

        return $this;
    }

    /**
     * Method to set the value of field timeofcheckout
     *
     * @param string $timeofcheckout
     * @return $this
     */
    public function setTimeofcheckout($timeofcheckout)
    {
        $this->timeofcheckout = $timeofcheckout;

        return $this;
    }

    /**
     * Method to set the value of field noofrooms
     *
     * @param integer $noofrooms
     * @return $this
     */
    public function setNoofrooms($noofrooms)
    {
        $this->noofrooms = $noofrooms;

        return $this;
    }

    /**
     * Method to set the value of field roomtype
     *
     * @param string $roomtype
     * @return $this
     */
    public function setRoomtype($roomtype)
    {
        $this->roomtype = $roomtype;

        return $this;
    }

    /**
     * Method to set the value of field noofbeds
     *
     * @param integer $noofbeds
     * @return $this
     */
    public function setNoofbeds($noofbeds)
    {
        $this->noofbeds = $noofbeds;

        return $this;
    }

    /**
     * Method to set the value of field cost
     *
     * @param string $cost
     * @return $this
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Method to set the value of field deposit
     *
     * @param string $deposit
     * @return $this
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;

        return $this;
    }

    /**
     * Method to set the value of field balance
     *
     * @param string $balance
     * @return $this
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Method to set the value of field datecheckin
     *
     * @param string $datecheckin
     * @return $this
     */
    public function setDatecheckin($datecheckin)
    {
        $this->datecheckin = $datecheckin;

        return $this;
    }

    /**
     * Method to set the value of field datecheckout
     *
     * @param string $datecheckout
     * @return $this
     */
    public function setDatecheckout($datecheckout)
    {
        $this->datecheckout = $datecheckout;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field standardpackageid
     *
     * @return integer
     */
    public function getStandardpackageid()
    {
        return $this->standardpackageid;
    }

    /**
     * Returns the value of field lodgingtypeid
     *
     * @return integer
     */
    public function getLodgingtypeid()
    {
        return $this->lodgingtypeid;
    }

    /**
     * Returns the value of field timeofcheckin
     *
     * @return string
     */
    public function getTimeofcheckin()
    {
        return $this->timeofcheckin;
    }

    /**
     * Returns the value of field timeofcheckout
     *
     * @return string
     */
    public function getTimeofcheckout()
    {
        return $this->timeofcheckout;
    }

    /**
     * Returns the value of field noofrooms
     *
     * @return integer
     */
    public function getNoofrooms()
    {
        return $this->noofrooms;
    }

    /**
     * Returns the value of field roomtype
     *
     * @return string
     */
    public function getRoomtype()
    {
        return $this->roomtype;
    }

    /**
     * Returns the value of field noofbeds
     *
     * @return integer
     */
    public function getNoofbeds()
    {
        return $this->noofbeds;
    }

    /**
     * Returns the value of field cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Returns the value of field deposit
     *
     * @return string
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Returns the value of field balance
     *
     * @return string
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Returns the value of field datecheckin
     *
     * @return string
     */
    public function getDatecheckin()
    {
        return $this->datecheckin;
    }

    /**
     * Returns the value of field datecheckout
     *
     * @return string
     */
    public function getDatecheckout()
    {
        return $this->datecheckout;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("lodginggroupbooking");
        $this->belongsTo('standardpackageid', 'occasion\Standardpackage', 'id', ['alias' => 'Standardpackage']);
        $this->belongsTo('lodgingtypeid', 'occasion\Lodgingtype', 'id', ['alias' => 'Lodgingtype']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'lodginggroupbooking';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Lodginggroupbooking[]|Lodginggroupbooking|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Lodginggroupbooking|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

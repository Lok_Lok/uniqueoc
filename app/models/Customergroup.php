<?php

namespace occasion;

class Customergroup extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $standardpackageid;

    /**
     *
     * @var string
     */
    protected $leadguestname;

    /**
     *
     * @var integer
     */
    protected $numberOfGuests;
	
    /**
     *
     * @var integer
     */
    protected $userid;

	
	public function __toString()
	{
		return $this->leadguestname . ", number of guests: " . $this->numberOfGuests;
	}

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field standardpackageid
     *
     * @param integer $standardpackageid
     * @return $this
     */
    public function setStandardpackageid($standardpackageid)
    {
        $this->standardpackageid = $standardpackageid;

        return $this;
    }

    /**
     * Method to set the value of field leadguestname
     *
     * @param string $leadguestname
     * @return $this
     */
    public function setLeadguestname($leadguestname)
    {
        $this->leadguestname = $leadguestname;

        return $this;
    }

    /**
     * Method to set the value of field numberOfGuests
     *
     * @param integer $numberOfGuests
     * @return $this
     */
    public function setNumberOfGuests($numberOfGuests)
    {
        $this->numberOfGuests = $numberOfGuests;

        return $this;
    }

    /**
     * Method to set the value of field userid
     *
     * @param integer $userid
     * @return $this
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field standardpackageid
     *
     * @return integer
     */
    public function getStandardpackageid()
    {
        return $this->standardpackageid;
    }

    /**
     * Returns the value of field leadguestname
     *
     * @return string
     */
    public function getLeadguestname()
    {
        return $this->leadguestname;
    }

    /**
     * Returns the value of field numberOfGuests
     *
     * @return integer
     */
    public function getNumberOfGuests()
    {
        return $this->numberOfGuests;
    }

    /**
     * Returns the value of field userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("customergroup");
        $this->hasMany('id', 'occasion\Customactivitypackage', 'customergroupid', ['alias' => 'Customactivitypackage']);
        $this->hasMany('id', 'occasion\Custombooking', 'customergroupid', ['alias' => 'Custombooking']);
        $this->hasMany('id', 'occasion\Groupjourneylog', 'customergroupid', ['alias' => 'Groupjourneylog']);
        $this->hasMany('id', 'occasion\Guest', 'customergroupid', ['alias' => 'Guest']);
        $this->belongsTo('userid', 'occasion\User', 'id', ['alias' => 'User']);
        $this->belongsTo('standardpackageid', 'occasion\Standardpackage', 'id', ['alias' => 'Standardpackage']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customergroup';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customergroup[]|Customergroup|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customergroup|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

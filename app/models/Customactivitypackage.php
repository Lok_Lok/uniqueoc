<?php

namespace occasion;

class Customactivitypackage extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $scheduledactivityid;

    /**
     *
     * @var integer
     */
    protected $userid;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field scheduledactivityid
     *
     * @param integer $scheduledactivityid
     * @return $this
     */
    public function setScheduledactivityid($scheduledactivityid)
    {
        $this->scheduledactivityid = $scheduledactivityid;

        return $this;
    }

    /**
     * Method to set the value of field userid
     *
     * @param integer $userid
     * @return $this
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field scheduledactivityid
     *
     * @return integer
     */
    public function getScheduledactivityid()
    {
        return $this->scheduledactivityid;
    }

    /**
     * Returns the value of field userid
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("customactivitypackage");
        $this->belongsTo('scheduledactivityid', 'occasion\Scheduledactivity', 'id', ['alias' => 'Scheduledactivity']);
        $this->belongsTo('userid', 'occasion\User', 'id', ['alias' => 'User']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'customactivitypackage';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customactivitypackage[]|Customactivitypackage|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Customactivitypackage|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

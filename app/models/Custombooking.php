<?php

namespace occasion;

class Custombooking extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $lodgingproviderid;

    /**
     *
     * @var integer
     */
    protected $customergroupid;

    /**
     *
     * @var string
     */
    protected $cost;

    /**
     *
     * @var string
     */
    protected $paymenttype;

    /**
     *
     * @var string
     */
    protected $deposit;

    /**
     *
     * @var string
     */
    protected $balance;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field lodgingproviderid
     *
     * @param integer $lodgingproviderid
     * @return $this
     */
    public function setLodgingproviderid($lodgingproviderid)
    {
        $this->lodgingproviderid = $lodgingproviderid;

        return $this;
    }

    /**
     * Method to set the value of field customergroupid
     *
     * @param integer $customergroupid
     * @return $this
     */
    public function setCustomergroupid($customergroupid)
    {
        $this->customergroupid = $customergroupid;

        return $this;
    }

    /**
     * Method to set the value of field cost
     *
     * @param string $cost
     * @return $this
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Method to set the value of field paymenttype
     *
     * @param string $paymenttype
     * @return $this
     */
    public function setPaymenttype($paymenttype)
    {
        $this->paymenttype = $paymenttype;

        return $this;
    }

    /**
     * Method to set the value of field deposit
     *
     * @param string $deposit
     * @return $this
     */
    public function setDeposit($deposit)
    {
        $this->deposit = $deposit;

        return $this;
    }

    /**
     * Method to set the value of field balance
     *
     * @param string $balance
     * @return $this
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field lodgingproviderid
     *
     * @return integer
     */
    public function getLodgingproviderid()
    {
        return $this->lodgingproviderid;
    }

    /**
     * Returns the value of field customergroupid
     *
     * @return integer
     */
    public function getCustomergroupid()
    {
        return $this->customergroupid;
    }

    /**
     * Returns the value of field cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Returns the value of field paymenttype
     *
     * @return string
     */
    public function getPaymenttype()
    {
        return $this->paymenttype;
    }

    /**
     * Returns the value of field deposit
     *
     * @return string
     */
    public function getDeposit()
    {
        return $this->deposit;
    }

    /**
     * Returns the value of field balance
     *
     * @return string
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("custombooking");
        $this->belongsTo('lodgingproviderid', 'occasion\Lodgingprovider', 'id', ['alias' => 'Lodgingprovider']);
        $this->belongsTo('customergroupid', 'occasion\Customergroup', 'id', ['alias' => 'Customergroup']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'custombooking';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Custombooking[]|Custombooking|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Custombooking|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

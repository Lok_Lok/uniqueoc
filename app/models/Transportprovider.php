<?php

namespace occasion;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;

class Transportprovider extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $companyname;

    /**
     *
     * @var string
     */
    protected $phoneno;

    /**
     *
     * @var string
     */
    protected $addressno;

    /**
     *
     * @var string
     */
    protected $streetname;

    /**
     *
     * @var string
     */
    protected $eircode;

    /**
     *
     * @var string
     */
    protected $city;

    /**
     *
     * @var string
     */
    protected $county;

    /**
     *
     * @var string
     */
    protected $email;

    /**
     *
     * @var string
     */
    protected $services;

    /**
     *
     * @var string
     */
    protected $vehicletypes;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field companyname
     *
     * @param string $companyname
     * @return $this
     */
    public function setCompanyname($companyname)
    {
        $this->companyname = $companyname;

        return $this;
    }

    /**
     * Method to set the value of field phoneno
     *
     * @param string $phoneno
     * @return $this
     */
    public function setPhoneno($phoneno)
    {
        $this->phoneno = $phoneno;

        return $this;
    }

    /**
     * Method to set the value of field addressno
     *
     * @param string $addressno
     * @return $this
     */
    public function setAddressno($addressno)
    {
        $this->addressno = $addressno;

        return $this;
    }

    /**
     * Method to set the value of field streetname
     *
     * @param string $streetname
     * @return $this
     */
    public function setStreetname($streetname)
    {
        $this->streetname = $streetname;

        return $this;
    }

    /**
     * Method to set the value of field eircode
     *
     * @param string $eircode
     * @return $this
     */
    public function setEircode($eircode)
    {
        $this->eircode = $eircode;

        return $this;
    }

    /**
     * Method to set the value of field city
     *
     * @param string $city
     * @return $this
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Method to set the value of field county
     *
     * @param string $county
     * @return $this
     */
    public function setCounty($county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field services
     *
     * @param string $services
     * @return $this
     */
    public function setServices($services)
    {
        $this->services = $services;

        return $this;
    }

    /**
     * Method to set the value of field vehicletypes
     *
     * @param string $vehicletypes
     * @return $this
     */
    public function setVehicletypes($vehicletypes)
    {
        $this->vehicletypes = $vehicletypes;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field companyname
     *
     * @return string
     */
    public function getCompanyname()
    {
        return $this->companyname;
    }

    /**
     * Returns the value of field phoneno
     *
     * @return string
     */
    public function getPhoneno()
    {
        return $this->phoneno;
    }

    /**
     * Returns the value of field addressno
     *
     * @return string
     */
    public function getAddressno()
    {
        return $this->addressno;
    }

    /**
     * Returns the value of field streetname
     *
     * @return string
     */
    public function getStreetname()
    {
        return $this->streetname;
    }

    /**
     * Returns the value of field eircode
     *
     * @return string
     */
    public function getEircode()
    {
        return $this->eircode;
    }

    /**
     * Returns the value of field city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Returns the value of field county
     *
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Returns the value of field services
     *
     * @return string
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * Returns the value of field vehicletypes
     *
     * @return string
     */
    public function getVehicletypes()
    {
        return $this->vehicletypes;
    }

	public function __toString()
	{
		return $this->companyname;
	}
    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'model'   => $this,
                    'message' => 'Please enter a correct email address',
                ]
            )
        );

        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("transportprovider");
        $this->hasMany('id', 'occasion\Journey', 'transportproviderid', ['alias' => 'Journey']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'transportprovider';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Transportprovider[]|Transportprovider|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Transportprovider|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

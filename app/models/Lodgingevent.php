<?php

class Lodgingevent extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Column(column="lodgingtypeid", type="integer", length=11, nullable=false)
     */
    protected $lodgingtypeid;

    /**
     *
     * @var string
     * @Column(column="title", type="string", length=50, nullable=true)
     */
    protected $title;

    /**
     *
     * @var string
     * @Column(column="cost", type="string", length=20, nullable=true)
     */
    protected $cost;

    /**
     *
     * @var integer
     * @Column(column="noofrooms", type="integer", length=9, nullable=true)
     */
    protected $noofrooms;

    /**
     *
     * @var integer
     * @Column(column="noofbeds", type="integer", length=9, nullable=true)
     */
    protected $noofbeds;

    /**
     *
     * @var string
     * @Column(column="start", type="string", length=21, nullable=true)
     */
    protected $start;

    /**
     *
     * @var string
     * @Column(column="end", type="string", length=21, nullable=true)
     */
    protected $end;

    /**
     *
     * @var string
     * @Column(column="lodginggroupbooking", type="string", length=24, nullable=false)
     */
    protected $lodginggroupbooking;

    /**
     *
     * @var integer
     * @Column(column="id", type="integer", length=8, nullable=false)
     */
    protected $id;

    /**
     * Method to set the value of field lodgingtypeid
     *
     * @param integer $lodgingtypeid
     * @return $this
     */
    public function setLodgingtypeid($lodgingtypeid)
    {
        $this->lodgingtypeid = $lodgingtypeid;

        return $this;
    }

    /**
     * Method to set the value of field title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Method to set the value of field cost
     *
     * @param string $cost
     * @return $this
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Method to set the value of field noofrooms
     *
     * @param integer $noofrooms
     * @return $this
     */
    public function setNoofrooms($noofrooms)
    {
        $this->noofrooms = $noofrooms;

        return $this;
    }

    /**
     * Method to set the value of field noofbeds
     *
     * @param integer $noofbeds
     * @return $this
     */
    public function setNoofbeds($noofbeds)
    {
        $this->noofbeds = $noofbeds;

        return $this;
    }

    /**
     * Method to set the value of field start
     *
     * @param string $start
     * @return $this
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Method to set the value of field end
     *
     * @param string $end
     * @return $this
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Method to set the value of field lodginggroupbooking
     *
     * @param string $lodginggroupbooking
     * @return $this
     */
    public function setLodginggroupbooking($lodginggroupbooking)
    {
        $this->lodginggroupbooking = $lodginggroupbooking;

        return $this;
    }

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Returns the value of field lodgingtypeid
     *
     * @return integer
     */
    public function getLodgingtypeid()
    {
        return $this->lodgingtypeid;
    }

    /**
     * Returns the value of field title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Returns the value of field cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Returns the value of field noofrooms
     *
     * @return integer
     */
    public function getNoofrooms()
    {
        return $this->noofrooms;
    }

    /**
     * Returns the value of field noofbeds
     *
     * @return integer
     */
    public function getNoofbeds()
    {
        return $this->noofbeds;
    }

    /**
     * Returns the value of field start
     *
     * @return string
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Returns the value of field end
     *
     * @return string
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Returns the value of field lodginggroupbooking
     *
     * @return string
     */
    public function getLodginggroupbooking()
    {
        return $this->lodginggroupbooking;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("lodgingevent");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'lodgingevent';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Lodgingevent[]|Lodgingevent|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Lodgingevent|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}

<?php

namespace occasion;

class Standardpackage extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=9, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=40, nullable=true)
     */
    protected $name;

    /**
     *
     * @var string
     * @Column(column="startdate", type="string", nullable=true)
     */
    protected $startdate;

    /**
     *
     * @var string
     * @Column(column="enddate", type="string", nullable=true)
     */
    protected $enddate;

    /**
     *
     * @var string
     * @Column(column="location", type="string", length=50, nullable=true)
     */
    protected $location;

    /**
     *
     * @var string
     * @Column(column="cost", type="string", length=30, nullable=true)
     */
    protected $cost;

    /**
     *
     * @var string
     * @Column(column="standardpackagepic", type="string", nullable=true)
     */
    protected $standardpackagepic;
	
	public function __toString()
	{
		return $this->name . " " . $this->location;
	}
    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field startdate
     *
     * @param string $startdate
     * @return $this
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Method to set the value of field enddate
     *
     * @param string $enddate
     * @return $this
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Method to set the value of field location
     *
     * @param string $location
     * @return $this
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Method to set the value of field cost
     *
     * @param string $cost
     * @return $this
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Method to set the value of field standardpackagepic
     *
     * @param string $standardpackagepic
     * @return $this
     */
    public function setStandardpackagepic($standardpackagepic)
    {
        $this->standardpackagepic = $standardpackagepic;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the value of field startdate
     *
     * @return string
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Returns the value of field enddate
     *
     * @return string
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Returns the value of field location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Returns the value of field cost
     *
     * @return string
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Returns the value of field standardpackagepic
     *
     * @return string
     */
    public function getStandardpackagepic()
    {
        return $this->standardpackagepic;
    }

    /**
     * Method to set the value of field rating
     *
     * @param integer $rating
     * @return $this
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Method to set the value of field packagepic
     *
     * @param string $packagepic
     * @return $this
     */
    public function setPackagepic($packagepic)
    {
        $this->packagepic = $packagepic;

        return $this;
    }

    /**
     * Returns the value of field rating
     *
     * @return integer
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Returns the value of field packagepic
     *
     * @return string
     */
    public function getPackagepic()
    {
        return $this->packagepic;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("standardpackage");
        $this->hasMany('id', 'occasion\Customergroup', 'standardpackageid', ['alias' => 'Customergroup']);
        $this->hasMany('id', 'occasion\Lodginggroupbooking', 'standardpackageid', ['alias' => 'Lodginggroupbooking']);
        $this->hasMany('id', 'occasion\Standardpackagelog', 'standardpackageid', ['alias' => 'Standardpackagelog']);
        $this->hasMany('id', 'occasion\Standardrating', 'standardpackageid', ['alias' => 'Standardrating']);

    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Standardpackage[]|Standardpackage|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Standardpackage|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'standardpackage';
    }


    public function getAverageRating()
    {
        $id = $this->id;
        $averageRating = StandardRating::average(['conditions' => "standardpackageid = $id",'column' => 'rating']);
        return $averageRating;
    }
}

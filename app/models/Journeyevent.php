<?php

class Journeyevent extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $journeyid;

    /**
     *
     * @var string
     */
    protected $title;

    /**
     *
     * @var string
     */
    protected $start;

    /**
     *
     * @var string
     */
    protected $groupjourneylog;

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     * Method to set the value of field journeyid
     *
     * @param integer $journeyid
     * @return $this
     */
    public function setJourneyid($journeyid)
    {
        $this->journeyid = $journeyid;

        return $this;
    }

    /**
     * Method to set the value of field title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Method to set the value of field start
     *
     * @param string $start
     * @return $this
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Method to set the value of field groupjourneylog
     *
     * @param string $groupjourneylog
     * @return $this
     */
    public function setGroupjourneylog($groupjourneylog)
    {
        $this->groupjourneylog = $groupjourneylog;

        return $this;
    }

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Returns the value of field journeyid
     *
     * @return integer
     */
    public function getJourneyid()
    {
        return $this->journeyid;
    }

    /**
     * Returns the value of field title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Returns the value of field start
     *
     * @return string
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Returns the value of field groupjourneylog
     *
     * @return string
     */
    public function getGroupjourneylog()
    {
        return $this->groupjourneylog;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSchema("occasion");
        $this->setSource("journeyevent");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'journeyevent';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return Journeyevent[]|Journeyevent|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Journeyevent|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

}
